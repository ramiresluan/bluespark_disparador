unit UCadastroUsuario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.DBCtrls, Vcl.StdCtrls, Vcl.Mask,
  Vcl.ExtCtrls, Data.DB, DBAccess, Uni,MsHtml,SHDocVw,Vcl.OleCtrls,MemDS,ActiveX;
  const
  IDM_MARCADOR = 2184;
  IDM_MARCADOR_LISTA = 2185;
  IDM_OUTDENT = 2187;
  IDM_INDENT = 2186;
  IDM_ALINHARESQ = 59;
  IDM_CENTRALIZAR = 57;
  IDM_ALINHADIR = 60;
  IDM_IMAGEM = 2168;
  IDM_LINHAHORIZ = 2150;
  IDM_RECORTAR = 16;
  IDM_COPIAR = 15;
  IDM_COLAR = 26;
  IDM_HYPERLINK = 2124;
  IDM_DESFAZER = 43;

type
  TCadastroUsuario = class(TForm)
    Panel4: TPanel;
    LbCadUsu: TLabel;
    EdSetf: TEdit;
    P1: TPanel;
    Label2: TLabel;
    Label10: TLabel;
    Label14: TLabel;
    LbAssinatura: TLabel;
    edNome: TDBEdit;
    edEmail: TDBEdit;
    btSalvar: TPanel;
    Label12: TLabel;
    btCancelar: TPanel;
    Label13: TLabel;
    DBESenha: TDBEdit;
    UdsUsuario: TUniDataSource;
    OpenDialog: TOpenDialog;
    WebBrowser1: TWebBrowser;
    PAssinatura: TPanel;
    Panel1: TPanel;
    Edit1: TEdit;
    PnNegrito: TPanel;
    PnItalico: TPanel;
    PnSublinhado: TPanel;
    PnCor: TPanel;
    PnEsquer: TPanel;
    PnCentro: TPanel;
    PnDireita: TPanel;
    PnFoto: TPanel;
    LbTamanho: TLabel;
    CbTamanho: TComboBox;
    ColorDialog: TColorDialog;
    procedure btSalvarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure DBIAssinaturaDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DocumentoEmBranco(WebBrowser: TWebBrowser);
    function GetIEHandle(WebBrowser: TWebbrowser; ClassName: string): HWND;
    procedure FormCreate(Sender: TObject);
    procedure PnNegritoClick(Sender: TObject);
    procedure PnItalicoClick(Sender: TObject);
    procedure PnSublinhadoClick(Sender: TObject);
    procedure PnEsquerClick(Sender: TObject);
    procedure PnCentroClick(Sender: TObject);
    procedure PnDireitaClick(Sender: TObject);
    procedure PnFotoClick(Sender: TObject);
    procedure CbTamanhoChange(Sender: TObject);
    procedure WBLoadHTML(WebBrowser: TWebBrowser; HTMLCode: string);
  private
    { Private declarations }
  public
    { Public declarations }
  caminho:String;
  end;

var
  CadastroUsuario: TCadastroUsuario;

implementation

uses UDM, UGeralPAF,Uprincipal;

{$R *.dfm}

procedure TCadastroUsuario.btCancelarClick(Sender: TObject);
begin
  dm.USUARIOS.Cancel;
  Close;
end;

procedure TCadastroUsuario.btSalvarClick(Sender: TObject);
var
  I: Integer;
  vString:TStringList;
begin
  if edNome.Text='' then
  begin
    GR.Aviso('Informe o NOME do cliente.');
    edNome.SetFocus;
    Exit;
  end;

  if Length(edNome.Text) < 2 then
  begin
    GR.Aviso('Nome menor que o permitido (2 d�gitos)! ');
    edNome.SetFocus;
    Exit;
  end;

  if edEmail.Text='' then
  begin
    GR.Aviso('Informe o Email.');
    edNome.SetFocus;
    Exit;
  end;

  if Length(edEmail.Text) < 2 then
  begin
    GR.Aviso('Email menor que o permitido (2 d�gitos)! ');
    edNome.SetFocus;
    Exit;
  end;

  if DBESenha.Text='' then
  begin
    GR.Aviso('Informe a senha.');
    edNome.SetFocus;
    Exit;
  end;

  if Length(DBESenha.Text) < 2 then
  begin
    GR.Aviso('Senha menor que o permitido (2 d�gitos)! ');
    edNome.SetFocus;
    Exit;
  end;

  dm.USUARIOSASSINATURA.AsString :=AnsiToUtf8(WebBrowser1.OleObject.Document.Body.InnerHTML);
  dm.USUARIOS.Post;

  Close;
end;

procedure TCadastroUsuario.CbTamanhoChange(Sender: TObject);
begin
    vHTMLDocumento := WebBrowser1.Document as IHTMLDocument2;
    case CbTamanho.ItemIndex of
      0: vHTMLDocumento.execCommand('FontSize', False,1);
      1: vHTMLDocumento.execCommand('FontSize', False,2);
      2: vHTMLDocumento.execCommand('FontSize', False,3);
      3: vHTMLDocumento.execCommand('FontSize', False,5);
      4: vHTMLDocumento.execCommand('FontSize', False,6);
      5: vHTMLDocumento.execCommand('FontSize', False,7);
    end;
end;

procedure TCadastroUsuario.DBIAssinaturaDblClick(Sender: TObject);
begin
    if OpenDialog.Execute then
  caminho := OpenDialog.FileName;
end;

procedure TCadastroUsuario.DocumentoEmBranco(WebBrowser: TWebBrowser);
begin
    WebBrowser.Navigate('about:blank');
end;

procedure TCadastroUsuario.FormCreate(Sender: TObject);
begin

//Navega em uma p�gina em Branco (About:Blank)
 DocumentoEmBranco(webbrowser1);
{altera o modo design do webbrowse para ON, ou seja, toda p�gina que abrirmos poderemos alterar, selecionar, excluir .. inserir ...}
 (WebBrowser1.Document as IHTMLDocument2).designMode := 'On';
end;

procedure TCadastroUsuario.FormShow(Sender: TObject);
var
   sHTML : string;
begin
  sHTML := '<html>'+
           '<body>'+
           '<br/>'+
           '<a></a>' +
           '<b>'+dm.USUARIOSASSINATURA.AsString+'</b>'+
           '</html>'+
           '</body>';
  WBLoadHTML(WebBrowser1, sHTML);
end;

function TCadastroUsuario.GetIEHandle(WebBrowser: TWebbrowser;
  ClassName: string): HWND;
var
hwndChild, hwndTmp: HWND;
oleCtrl: TOleControl;
szClass: array [0..255] of char;

begin
oleCtrl :=WebBrowser;
hwndTmp := oleCtrl.Handle;
    while (true) do
        begin
        hwndChild := GetWindow(hwndTmp, GW_CHILD);
        GetClassName(hwndChild, szClass, SizeOf(szClass));
            if (string(szClass)=ClassName) then
              begin
              Result :=hwndChild;
              Exit;
              end;
        hwndTmp := hwndChild;
        end;

        Result := 0;
end;

procedure TCadastroUsuario.PnCentroClick(Sender: TObject);
begin
    SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
    WM_COMMAND,IDM_CENTRALIZAR, 0);
end;

procedure TCadastroUsuario.PnDireitaClick(Sender: TObject);
begin
    SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
    WM_COMMAND,IDM_ALINHADIR, 0);
end;

procedure TCadastroUsuario.PnEsquerClick(Sender: TObject);
begin
    SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
    WM_COMMAND,IDM_ALINHARESQ, 0);
end;

procedure TCadastroUsuario.PnFotoClick(Sender: TObject);
begin
    SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
    WM_COMMAND,IDM_IMAGEM, 0);
end;

procedure TCadastroUsuario.PnItalicoClick(Sender: TObject);
begin
   BorderStyle    := bsSingle;
   vHTMLDocumento := WebBrowser1.Document as IHTMLDocument2;
   vHTMLDocumento.execCommand('Italic', False,0);
end;

procedure TCadastroUsuario.PnNegritoClick(Sender: TObject);
begin
   BorderStyle    := bsSingle;
{Aqui estamos dizendo que HTMLDocumento = A Interface que comanda o design do webbrowser, a interface que ir� inserir em seu texto selecionado um negrito... italico..}
   vHTMLDocumento := WebBrowser1.Document as IHTMLDocument2;

{Estamos falando oq queremos aplicar no texto selecionado, ent�o dizemos Bold ou seja Negrito}
   vHTMLDocumento.execCommand('Bold', False,0);
end;

procedure TCadastroUsuario.PnSublinhadoClick(Sender: TObject);
begin
    vHTMLDocumento := WebBrowser1.Document as IHTMLDocument2;
    vHTMLDocumento.execCommand('Underline', False,0);
end;

procedure TCadastroUsuario.WBLoadHTML(WebBrowser: TWebBrowser;
  HTMLCode: string);
var
   sl: TStringList;
   ms: TMemoryStream;
begin
   WebBrowser1.Navigate('about:blank') ;
   while WebBrowser1.ReadyState < READYSTATE_INTERACTIVE do
    Application.ProcessMessages;
   (WebBrowser1.Document as IHTMLDocument2).designMode := 'On';
   if Assigned(WebBrowser1.Document) then
   begin
     sl := TStringList.Create;
     try
       ms := TMemoryStream.Create;
       try
         sl.Text := HTMLCode;
         sl.SaveToStream(ms) ;
         ms.Seek(0, 0) ;
         (WebBrowser1.Document as IPersistStreamInit).Load(TStreamAdapter.Create(ms)) ;
       finally
         ms.Free;
       end;
     finally
       sl.Free;
     end;
   end;
end;

end.
