unit UAnexos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.ExtCtrls, Data.DB,
  Vcl.Grids, vcl.wwdbigrd, vcl.wwdbgrid, Vcl.StdCtrls;

type
  TFCadastroAnexo = class(TForm)
    Grid: TwwDBGrid;
    dsmAnexos: TDataSource;
    Panel1: TPanel;
    sbDeleteArquivo: TSpeedButton;
    sbAddArquivo: TSpeedButton;
    Opd: TOpenDialog;
    Panel2: TPanel;
    Panel4: TPanel;
    LbCadUsu: TLabel;
    EdSetf: TEdit;
    SbFechar: TSpeedButton;
    procedure sbAddArquivoClick(Sender: TObject);
    procedure sbDeleteArquivoClick(Sender: TObject);
    procedure Panel2Click(Sender: TObject);
    procedure SbFecharClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroAnexo: TFCadastroAnexo;

implementation

{$R *.dfm}

uses UDm;

procedure TFCadastroAnexo.Panel2Click(Sender: TObject);
begin
 close;
end;

procedure TFCadastroAnexo.sbAddArquivoClick(Sender: TObject);
begin
  if Opd.Execute then
  begin
    dm.mAnexos.Append;
    dm.mAnexosPATH.AsString:=Opd.FileName;
    dm.mAnexos.Post;
  end;
end;

procedure TFCadastroAnexo.sbDeleteArquivoClick(Sender: TObject);
begin
  dm.mAnexos.Delete;
end;

procedure TFCadastroAnexo.SbFecharClick(Sender: TObject);
begin
 Close;
end;

end.
