unit UCadastroClientes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, DB, DBCtrls, Mask, Buttons, jpeg, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit, DBAccess, Uni ;

type
  TFCadastroClientes = class(TForm)
    P1: TPanel;
    edNome: TDBEdit;
    edTelefone1: TDBEdit;
    edEmail: TDBEdit;
    MMObservacao: TDBMemo;
    Label2: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label5: TLabel;
    lkGRUPO: TDBLookupComboBox;
    btSalvar: TPanel;
    Label12: TLabel;
    btCancelar: TPanel;
    Label13: TLabel;
    Panel4: TPanel;
    LbAbrirCaixa: TLabel;
    EdSetf: TEdit;
    DBEdit1: TDBEdit;
    UdsCategoria: TUniDataSource;
    UdSClientes: TUniDataSource;
    procedure btCancelarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);

    function  CamposEmBranco: Boolean;
    procedure BitBtn1Click(Sender: TObject);

    procedure EdInscEstadualKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure edTelefone1KeyPress(Sender: TObject; var Key: Char);
    procedure AtualizarTelaCli;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroClientes: TFCadastroClientes;

implementation

uses UDM, UGeralPAF;

{$R *.dfm}


procedure TFCadastroClientes.EdInscEstadualKeyPress(Sender: TObject;
  var Key: Char);
begin
   if not (key in ['0'..'9',#8]) then
    key:=#0;
end;

procedure TFCadastroClientes.edTelefone1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (CharInSet(key,[#8,#13,#44,'0'..'9'])) then
  key:=#0;
end;

function TFCadastroClientes.CamposEmBranco: Boolean;
begin
  Result:=True;

  if edNome.Text='' then
  begin
      GR.Aviso('INFORME O NOME!');
      edNome.SetFocus;
      Exit;
  end
  Else if Length(edNome.Text) < 2 then
  begin
    GR.Aviso('Nome menor que o permitido (2 d�gitos)! ');
    edNome.SetFocus;
    Exit;
  end;

  Result:=False;
end;


procedure TFCadastroClientes.AtualizarTelaCli;
begin
    dm.CLIENTES.Close;
    dm.CLIENTES.SQL.Clear;
    dm.CLIENTES.SQL.Add('SELECT * FROM ClIENTE');
    dm.CLIENTES.SQL.Add('ORDER BY CAST(CLIENTEID AS INTEGER)');
    dm.CLIENTES.Open;
end;

procedure TFCadastroClientes.BitBtn1Click(Sender: TObject);
begin
  Close;
end;


procedure TFCadastroClientes.btCancelarClick(Sender: TObject);
begin
  AtualizarTelaCli;
  Close;
end;

procedure TFCadastroClientes.btSalvarClick(Sender: TObject);
begin
  if edNome.Text='' then
  begin
    GR.Aviso('Informe o NOME do cliente.');
    edNome.SetFocus;
    Exit;
  end
  Else if Length(edNome.Text) < 2 then
  begin
    GR.Aviso('Nome menor que o permitido (2 d�gitos)! ');
    edNome.SetFocus;
    Exit;
  end;

    if edEmail.Text='' then
  begin
    GR.Aviso('Informe o Email do cliente.');
    edEmail.SetFocus;
    Exit;
  end
  Else if Length(edEmail.Text) < 2 then
  begin
    GR.Aviso('Email menor que o permitido (2 d�gitos)! ');
    edNome.SetFocus;
    Exit;
  end;

    if edTelefone1.Text='' then
  begin
    GR.Aviso('Informe o Telefone do cliente.');
    edNome.SetFocus;
    Exit;
  end
  Else if Length(edTelefone1.Text) < 8 then
  begin
    GR.Aviso('Telefone menor que o permitido (8 d�gitos)! ');
    edNome.SetFocus;
    Exit;
  end;

  if dm.CLIENTES.State in [dsedit] then
  begin
      dm.CLIENTESCATEGORIA.AsString :=lkGRUPO.Text;
      dm.Clientes.Post;
  end
  else
  begin
      dm.CLIENTESCATEGORIA.AsString :=lkGRUPO.Text;
      dm.CLIENTESNOME.AsString      :=edNome.Text;
      dm.CLIENTESEMAIL.AsString     :=edEmail.Text;
      dm.CLIENTESPERSON.AsString    :=DBEdit1.Text;
      dm.CLIENTESOBS.AsString       :=MMObservacao.Text;
      dm.CLIENTESCATEGORIA.AsString :=lkGRUPO.Text;
      dm.Clientes.Post;
  end;
  Close;
end;

procedure TFCadastroClientes.FormCreate(Sender: TObject);
begin
  // dm.CATEGORIA.Close;
  // dm.CATEGORIA.Open;
end;

procedure TFCadastroClientes.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
  if key=VK_F1     then btSalvarClick(self);
  if key=VK_F2     then btCancelarClick(self);
end;

procedure TFCadastroClientes.FormShow(Sender: TObject);
begin
   edNome.SetFocus;
end;

end.
