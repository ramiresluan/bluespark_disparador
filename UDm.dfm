object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 310
  Width = 465
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    ScreenCursor = gcrHourGlass
    Left = 251
    Top = 8
  end
  object FDPhysODBCDriverLink1: TFDPhysODBCDriverLink
    ODBCDriver = 'SQL Server'
    Left = 251
    Top = 64
  end
  object FDMoniRemoteClientLink1: TFDMoniRemoteClientLink
    Left = 251
    Top = 124
  end
  object IdFTP: TIdFTP
    IPVersion = Id_IPv4
    Host = 'ftp.servidortotal.com.br'
    Passive = True
    ConnectTimeout = 0
    Password = 'joaoftp123'
    Username = 'servidortotal'
    NATKeepAlive.UseKeepAlive = False
    NATKeepAlive.IdleTimeMS = 0
    NATKeepAlive.IntervalMS = 0
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    ReadTimeout = 5000
    Left = 248
    Top = 182
  end
  object SQLServer: TUniConnection
    ProviderName = 'SQL Server'
    Port = 8081
    Database = 'GreatOcean'
    Username = 'inffel'
    Server = '177.12.2.130,1433'
    LoginPrompt = False
    Left = 24
    Top = 8
    EncryptedPassword = 'D5FF96FF91FFB9FFB9FFCCFF93FFD9FFCDFFCFFFCEFFC9FF'
  end
  object SQLServerUniProvider1: TSQLServerUniProvider
    Left = 112
    Top = 8
  end
  object USUARIOS: TUniQuery
    SQLInsert.Strings = (
      'INSERT INTO dbo.USUARIO'
      '  (NOME, SENHA, ASSINATURA, EMAIL, LOGO)'
      'VALUES'
      '  (:NOME, :SENHA, :ASSINATURA, :EMAIL, :LOGO)'
      'SET :USUARIOID = SCOPE_IDENTITY()')
    SQLDelete.Strings = (
      'DELETE FROM dbo.USUARIO'
      'WHERE'
      '  USUARIOID = :Old_USUARIOID')
    SQLUpdate.Strings = (
      'UPDATE dbo.USUARIO'
      'SET'
      
        '  NOME = :NOME, SENHA = :SENHA, ASSINATURA = :ASSINATURA, EMAIL ' +
        '= :EMAIL, LOGO = :LOGO'
      'WHERE'
      '  USUARIOID = :Old_USUARIOID')
    SQLLock.Strings = (
      'SELECT * FROM dbo.USUARIO'
      'WITH (UPDLOCK, ROWLOCK, HOLDLOCK)'
      'WHERE'
      '  USUARIOID = :Old_USUARIOID')
    SQLRefresh.Strings = (
      'SELECT NOME, SENHA, ASSINATURA, EMAIL, LOGO FROM dbo.USUARIO'
      'WHERE'
      '  USUARIOID = :USUARIOID')
    SQLRecCount.Strings = (
      'SET :PCOUNT = (SELECT COUNT(*) FROM dbo.USUARIO'
      ')')
    Connection = SQLServer
    SQL.Strings = (
      'Select * from dbo.USUARIO')
    Left = 24
    Top = 128
    object USUARIOSUSUARIOID: TIntegerField
      FieldName = 'USUARIOID'
      ReadOnly = True
      Required = True
    end
    object USUARIOSNOME: TStringField
      FieldName = 'NOME'
      Required = True
      Size = 50
    end
    object USUARIOSSENHA: TStringField
      FieldName = 'SENHA'
      Required = True
    end
    object USUARIOSASSINATURA: TMemoField
      FieldName = 'ASSINATURA'
      BlobType = ftMemo
    end
    object USUARIOSEMAIL: TStringField
      FieldName = 'EMAIL'
      Required = True
      Size = 25
    end
    object USUARIOSLOGO: TMemoField
      FieldName = 'LOGO'
      BlobType = ftMemo
    end
  end
  object AGENDAMENTO: TUniQuery
    SQLInsert.Strings = (
      'INSERT INTO dbo.AGENDAMENTO'
      '  (DATA_ENVIO, HORA_ENVIO, DATA_AGENDA, HORA_AGENDA, ENVIOID)'
      'VALUES'
      
        '  (:DATA_ENVIO, :HORA_ENVIO, :DATA_AGENDA, :HORA_AGENDA, :ENVIOI' +
        'D)'
      'SET :AGENDAMENTOID = SCOPE_IDENTITY()')
    SQLDelete.Strings = (
      'DELETE FROM dbo.AGENDAMENTO'
      'WHERE'
      '  AGENDAMENTOID = :Old_AGENDAMENTOID')
    SQLUpdate.Strings = (
      'UPDATE dbo.AGENDAMENTO'
      'SET'
      
        '  DATA_ENVIO = :DATA_ENVIO, HORA_ENVIO = :HORA_ENVIO, DATA_AGEND' +
        'A = :DATA_AGENDA, HORA_AGENDA = :HORA_AGENDA, ENVIOID = :ENVIOID'
      'WHERE'
      '  AGENDAMENTOID = :Old_AGENDAMENTOID')
    SQLLock.Strings = (
      'SELECT * FROM dbo.AGENDAMENTO'
      'WITH (UPDLOCK, ROWLOCK, HOLDLOCK)'
      'WHERE'
      '  AGENDAMENTOID = :Old_AGENDAMENTOID')
    SQLRefresh.Strings = (
      
        'SELECT DATA_ENVIO, HORA_ENVIO, DATA_AGENDA, HORA_AGENDA, ENVIOID' +
        ' FROM dbo.AGENDAMENTO'
      'WHERE'
      '  AGENDAMENTOID = :AGENDAMENTOID')
    SQLRecCount.Strings = (
      'SET :PCOUNT = (SELECT COUNT(*) FROM dbo.AGENDAMENTO'
      ')')
    Connection = SQLServer
    SQL.Strings = (
      'Select * from dbo.AGENDAMENTO')
    Left = 112
    Top = 72
    object AGENDAMENTOAGENDAMENTOID: TIntegerField
      FieldName = 'AGENDAMENTOID'
      ReadOnly = True
      Required = True
    end
    object AGENDAMENTODATA_ENVIO: TDateField
      FieldName = 'DATA_ENVIO'
    end
    object AGENDAMENTOHORA_ENVIO: TDateTimeField
      FieldName = 'HORA_ENVIO'
    end
    object AGENDAMENTODATA_AGENDA: TDateField
      FieldName = 'DATA_AGENDA'
    end
    object AGENDAMENTOHORA_AGENDA: TDateTimeField
      FieldName = 'HORA_AGENDA'
    end
    object AGENDAMENTOENVIOID: TIntegerField
      FieldName = 'ENVIOID'
      Required = True
    end
  end
  object ENVIOS: TUniQuery
    SQLInsert.Strings = (
      'INSERT INTO dbo.ENVIOS'
      
        '  (DATA, HORA, USUARIO, CORPOEMAIL, ENVIADOS, ASSUNTO, EMAILUSU,' +
        ' CLIENTEID, PERSON, EMAILS, CATEGORIA, ANEXOS, HORAENVIO, ID_ANE' +
        'XO)'
      'VALUES'
      
        '  (:DATA, :HORA, :USUARIO, :CORPOEMAIL, :ENVIADOS, :ASSUNTO, :EM' +
        'AILUSU, :CLIENTEID, :PERSON, :EMAILS, :CATEGORIA, :ANEXOS, :HORA' +
        'ENVIO, :ID_ANEXO)'
      'SET :ENVIOID = SCOPE_IDENTITY()')
    SQLDelete.Strings = (
      'DELETE FROM dbo.ENVIOS'
      'WHERE'
      '  ENVIOID = :Old_ENVIOID')
    SQLUpdate.Strings = (
      'UPDATE dbo.ENVIOS'
      'SET'
      
        '  DATA = :DATA, HORA = :HORA, USUARIO = :USUARIO, CORPOEMAIL = :' +
        'CORPOEMAIL, ENVIADOS = :ENVIADOS, ASSUNTO = :ASSUNTO, EMAILUSU =' +
        ' :EMAILUSU, CLIENTEID = :CLIENTEID, PERSON = :PERSON, EMAILS = :' +
        'EMAILS, CATEGORIA = :CATEGORIA, ANEXOS = :ANEXOS, HORAENVIO = :H' +
        'ORAENVIO, ID_ANEXO = :ID_ANEXO'
      'WHERE'
      '  ENVIOID = :Old_ENVIOID')
    SQLLock.Strings = (
      'SELECT * FROM dbo.ENVIOS'
      'WITH (UPDLOCK, ROWLOCK, HOLDLOCK)'
      'WHERE'
      '  ENVIOID = :Old_ENVIOID')
    SQLRefresh.Strings = (
      
        'SELECT DATA, HORA, USUARIO, CORPOEMAIL, ENVIADOS, ASSUNTO, EMAIL' +
        'USU, CLIENTEID, PERSON, EMAILS, CATEGORIA, ANEXOS, HORAENVIO, ID' +
        '_ANEXO FROM dbo.ENVIOS'
      'WHERE'
      '  ENVIOID = :ENVIOID')
    SQLRecCount.Strings = (
      'SET :PCOUNT = (SELECT COUNT(*) FROM dbo.ENVIOS'
      ')')
    Connection = SQLServer
    SQL.Strings = (
      'Select * from dbo.ENVIOS '
      'WHERE ENVIOID= -1')
    Left = 112
    Top = 128
    object ENVIOSENVIOID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ENVIOID'
      ReadOnly = True
      Required = True
    end
    object ENVIOSDATA: TDateField
      FieldName = 'DATA'
    end
    object ENVIOSHORA: TTimeField
      FieldName = 'HORA'
    end
    object ENVIOSUSUARIO: TIntegerField
      FieldName = 'USUARIO'
    end
    object ENVIOSCORPOEMAIL: TWideMemoField
      FieldName = 'CORPOEMAIL'
      BlobType = ftWideMemo
    end
    object ENVIOSENVIADOS: TStringField
      FieldName = 'ENVIADOS'
      FixedChar = True
      Size = 1
    end
    object ENVIOSASSUNTO: TWideStringField
      FieldName = 'ASSUNTO'
      Size = 50
    end
    object ENVIOSEMAILUSU: TWideStringField
      FieldName = 'EMAILUSU'
    end
    object ENVIOSCLIENTEID: TIntegerField
      FieldName = 'CLIENTEID'
    end
    object ENVIOSPERSON: TWideStringField
      FieldName = 'PERSON'
      Size = 50
    end
    object ENVIOSEMAILS: TWideStringField
      FieldName = 'EMAILS'
      Size = 50
    end
    object ENVIOSCATEGORIA: TWideStringField
      FieldName = 'CATEGORIA'
    end
    object ENVIOSANEXOS: TWideMemoField
      FieldName = 'ANEXOS'
      BlobType = ftWideMemo
    end
    object ENVIOSHORAENVIO: TTimeField
      FieldName = 'HORAENVIO'
    end
    object ENVIOSID_ANEXO: TLargeintField
      FieldName = 'ID_ANEXO'
    end
  end
  object CATEGORIA: TUniQuery
    SQLInsert.Strings = (
      'INSERT INTO dbo.CATEGORIA'
      '  (CATEGORIA)'
      'VALUES'
      '  (:CATEGORIA)'
      'SET :LISTAID = SCOPE_IDENTITY()')
    SQLDelete.Strings = (
      'DELETE FROM dbo.CATEGORIA'
      'WHERE'
      '  LISTAID = :Old_LISTAID')
    SQLUpdate.Strings = (
      'UPDATE dbo.CATEGORIA'
      'SET'
      '  CATEGORIA = :CATEGORIA'
      'WHERE'
      '  LISTAID = :Old_LISTAID')
    SQLLock.Strings = (
      'SELECT * FROM dbo.CATEGORIA'
      'WITH (UPDLOCK, ROWLOCK, HOLDLOCK)'
      'WHERE'
      '  LISTAID = :Old_LISTAID')
    SQLRefresh.Strings = (
      'SELECT CATEGORIA FROM dbo.CATEGORIA'
      'WHERE'
      '  LISTAID = :LISTAID')
    SQLRecCount.Strings = (
      'SET :PCOUNT = (SELECT COUNT(*) FROM dbo.CATEGORIA'
      ')')
    Connection = SQLServer
    SQL.Strings = (
      'Select * from dbo.CATEGORIA')
    Left = 24
    Top = 176
    object CATEGORIALISTAID: TIntegerField
      FieldName = 'LISTAID'
      ReadOnly = True
      Required = True
    end
    object CATEGORIACATEGORIA: TStringField
      FieldName = 'CATEGORIA'
    end
  end
  object AchaCliente: TUniQuery
    SQLInsert.Strings = (
      'INSERT INTO DBO.CLIENTE'
      '  (NOME, EMAIL, CATEGORIA, PERSON, TELEFONE, OBS)'
      'VALUES'
      '  (:NOME, :EMAIL, :CATEGORIA, :PERSON, :TELEFONE, :OBS)'
      'SET :CLIENTEID = SCOPE_IDENTITY()')
    SQLDelete.Strings = (
      'DELETE FROM DBO.CLIENTE'
      'WHERE'
      '  CLIENTEID = :Old_CLIENTEID')
    SQLUpdate.Strings = (
      'UPDATE DBO.CLIENTE'
      'SET'
      
        '  NOME = :NOME, EMAIL = :EMAIL, CATEGORIA = :CATEGORIA, PERSON =' +
        ' :PERSON, TELEFONE = :TELEFONE, OBS = :OBS'
      'WHERE'
      '  CLIENTEID = :Old_CLIENTEID')
    SQLLock.Strings = (
      'SELECT * FROM DBO.CLIENTE'
      'WITH (UPDLOCK, ROWLOCK, HOLDLOCK)'
      'WHERE'
      '  CLIENTEID = :Old_CLIENTEID')
    SQLRefresh.Strings = (
      
        'SELECT NOME, EMAIL, CATEGORIA, PERSON, TELEFONE, OBS FROM DBO.CL' +
        'IENTE'
      'WHERE'
      '  CLIENTEID = :CLIENTEID')
    SQLRecCount.Strings = (
      'SET :PCOUNT = (SELECT COUNT(*) FROM DBO.CLIENTE'
      ')')
    Connection = SQLServer
    SQL.Strings = (
      'SELECT * FROM DBO.CLIENTE '
      ' WHERE NOME LIKE :PARAMETRO')
    Left = 112
    Top = 185
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PARAMETRO'
        Value = nil
      end>
    object AchaClienteCLIENTEID: TIntegerField
      FieldName = 'CLIENTEID'
      ReadOnly = True
      Required = True
    end
    object AchaClienteNOME: TStringField
      FieldName = 'NOME'
      Size = 50
    end
    object AchaClienteEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 50
    end
    object AchaClienteCATEGORIA: TStringField
      FieldName = 'CATEGORIA'
      Size = 50
    end
    object AchaClientePERSON: TStringField
      FieldName = 'PERSON'
      Size = 50
    end
    object AchaClienteTELEFONE: TStringField
      FieldName = 'TELEFONE'
      Size = 50
    end
    object AchaClienteOBS: TMemoField
      FieldName = 'OBS'
      BlobType = ftMemo
    end
  end
  object CLIENTES: TUniTable
    TableName = 'CLIENTE'
    Connection = SQLServer
    Left = 24
    Top = 72
    object CLIENTESCATEGORIA: TStringField
      DisplayWidth = 30
      FieldName = 'CATEGORIA'
      Size = 50
    end
    object CLIENTESNOME: TStringField
      DisplayWidth = 30
      FieldName = 'NOME'
      Size = 50
    end
    object CLIENTESEMAIL: TStringField
      DisplayWidth = 30
      FieldName = 'EMAIL'
      Size = 50
    end
    object CLIENTESSELECIONAR: TIntegerField
      DisplayWidth = 1
      FieldName = 'SELECIONAR'
    end
    object CLIENTESCLIENTEID: TIntegerField
      DisplayWidth = 10
      FieldName = 'CLIENTEID'
      ReadOnly = True
      Required = True
      Visible = False
    end
    object CLIENTESPERSON: TStringField
      DisplayWidth = 50
      FieldName = 'PERSON'
      Visible = False
      Size = 50
    end
    object CLIENTESTELEFONE: TStringField
      DisplayWidth = 50
      FieldName = 'TELEFONE'
      Visible = False
      Size = 50
    end
    object CLIENTESOBS: TMemoField
      DisplayWidth = 10
      FieldName = 'OBS'
      Visible = False
      BlobType = ftMemo
    end
  end
  object UniCategorias: TUniQuery
    SQLInsert.Strings = (
      'INSERT INTO CATEGORIA'
      '  (CATEGORIA)'
      'VALUES'
      '  (:CATEGORIA)'
      'SET :LISTAID = SCOPE_IDENTITY()')
    SQLDelete.Strings = (
      'DELETE FROM CATEGORIA'
      'WHERE'
      '  LISTAID = :Old_LISTAID')
    SQLUpdate.Strings = (
      'UPDATE CATEGORIA'
      'SET'
      '  CATEGORIA = :CATEGORIA'
      'WHERE'
      '  LISTAID = :Old_LISTAID')
    SQLLock.Strings = (
      'SELECT * FROM CATEGORIA'
      'WITH (UPDLOCK, ROWLOCK, HOLDLOCK)'
      'WHERE'
      '  LISTAID = :Old_LISTAID')
    SQLRefresh.Strings = (
      'SELECT CATEGORIA FROM CATEGORIA'
      'WHERE'
      '  LISTAID = :LISTAID')
    SQLRecCount.Strings = (
      'SET :PCOUNT = (SELECT COUNT(*) FROM CATEGORIA'
      ')')
    Connection = SQLServer
    SQL.Strings = (
      'SELECT 0 as XCHECK,CATEGORIA FROM CATEGORIA')
    Left = 356
    Top = 12
    object UniCategoriasXCHECK: TIntegerField
      DisplayLabel = '#'
      DisplayWidth = 3
      FieldName = 'XCHECK'
    end
    object UniCategoriasCATEGORIA: TStringField
      DisplayWidth = 40
      FieldName = 'CATEGORIA'
    end
  end
  object UniClientes: TUniQuery
    SQLInsert.Strings = (
      'INSERT INTO cliente'
      '  (NOME, EMAIL, CATEGORIA, PERSON)'
      'VALUES'
      '  (:NOME, :EMAIL, :CATEGORIA, :PERSON)'
      'SET :CLIENTEID = SCOPE_IDENTITY()')
    SQLDelete.Strings = (
      'DELETE FROM cliente'
      'WHERE'
      '  CLIENTEID = :Old_CLIENTEID')
    SQLUpdate.Strings = (
      'UPDATE cliente'
      'SET'
      
        '  NOME = :NOME, EMAIL = :EMAIL, CATEGORIA = :CATEGORIA, PERSON =' +
        ' :PERSON'
      'WHERE'
      '  CLIENTEID = :Old_CLIENTEID')
    SQLLock.Strings = (
      'SELECT * FROM cliente'
      'WITH (UPDLOCK, ROWLOCK, HOLDLOCK)'
      'WHERE'
      '  CLIENTEID = :Old_CLIENTEID')
    SQLRefresh.Strings = (
      'SELECT NOME, EMAIL, CATEGORIA, PERSON FROM cliente'
      'WHERE'
      '  CLIENTEID = :CLIENTEID')
    SQLRecCount.Strings = (
      'SET :PCOUNT = (SELECT COUNT(*) FROM cliente'
      ')')
    Connection = SQLServer
    SQL.Strings = (
      
        'SELECT 0 as XCHECK,NOME,CATEGORIA,EMAIL,PERSON,CLIENTEID FROM cl' +
        'iente')
    Left = 356
    Top = 76
    object UniClientesCATEGORIA: TStringField
      DisplayWidth = 30
      FieldName = 'CATEGORIA'
      Size = 50
    end
    object UniClientesNOME: TStringField
      DisplayWidth = 50
      FieldName = 'NOME'
      Size = 50
    end
    object UniClientesEMAIL: TStringField
      DisplayWidth = 43
      FieldName = 'EMAIL'
      Size = 50
    end
    object UniClientesXCHECK: TIntegerField
      FieldName = 'XCHECK'
    end
    object UniClientesCLIENTEID: TIntegerField
      FieldName = 'CLIENTEID'
      ReadOnly = True
      Required = True
    end
    object UniClientesPERSON: TStringField
      FieldName = 'PERSON'
      Size = 50
    end
  end
  object cEnvios: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'ENVIOID'
        Attributes = [faRequired]
        DataType = ftLargeint
      end
      item
        Name = 'DATA'
        DataType = ftDate
      end
      item
        Name = 'HORA'
        DataType = ftTime
      end
      item
        Name = 'USUARIO'
        DataType = ftInteger
      end
      item
        Name = 'CORPOEMAIL'
        DataType = ftWideMemo
      end
      item
        Name = 'ENVIADOS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ASSUNTO'
        DataType = ftWideString
        Size = 50
      end
      item
        Name = 'EMAILUSU'
        DataType = ftWideString
        Size = 20
      end
      item
        Name = 'CLIENTEID'
        DataType = ftInteger
      end
      item
        Name = 'PERSON'
        DataType = ftWideString
        Size = 50
      end
      item
        Name = 'EMAILS'
        DataType = ftWideString
        Size = 50
      end
      item
        Name = 'CATEGORIA'
        DataType = ftWideString
        Size = 20
      end
      item
        Name = 'ANEXOS'
        DataType = ftWideMemo
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    FormatOptions.AssignedValues = [fvMaxBcdPrecision, fvMaxBcdScale]
    FormatOptions.MaxBcdPrecision = 2147483647
    FormatOptions.MaxBcdScale = 2147483647
    ResourceOptions.AssignedValues = [rvPersistent, rvSilentMode]
    ResourceOptions.Persistent = True
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvUpdateChngFields, uvUpdateMode, uvLockMode, uvLockPoint, uvLockWait, uvRefreshMode, uvFetchGeneratorsPoint, uvCheckRequired, uvCheckReadOnly, uvCheckUpdatable, uvAutoCommitUpdates]
    UpdateOptions.LockWait = True
    UpdateOptions.FetchGeneratorsPoint = gpNone
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 360
    Top = 128
    Content = {
      414442530F006A4350050000FF00010001FF02FF0304000E000000630045006E
      00760069006F00730005000A0000005400610062006C00650006000000000007
      0000080032000000090000FF0AFF0B04000E00000045004E00560049004F0049
      00440005000E00000045004E00560049004F00490044000C00010000000E000D
      000F000110000111000112000113000114000115000E00000045004E00560049
      004F0049004400FEFF0B04000800000044004100540041000500080000004400
      4100540041000C00020000000E0016000F000117000111000118000112000113
      00011500080000004400410054004100FEFF0B04000800000048004F00520041
      0005000800000048004F00520041000C00030000000E0019000F000117000111
      000118000112000113000115000800000048004F0052004100FEFF0B04000E00
      00005500530055004100520049004F0005000E00000055005300550041005200
      49004F000C00040000000E001A000F0001170001110001180001120001130001
      15000E0000005500530055004100520049004F00FEFF0B04001400000043004F
      00520050004F0045004D00410049004C0005001400000043004F00520050004F
      0045004D00410049004C000C00050000000E001B001700011C00011100011800
      0112000113000115001400000043004F00520050004F0045004D00410049004C
      00FEFF0B04001000000045004E0056004900410044004F005300050010000000
      45004E0056004900410044004F0053000C00060000000E001D001E0001000000
      0F000117000111000118000112000113000115001000000045004E0056004900
      410044004F0053001F0001000000FEFF0B04000E00000041005300530055004E
      0054004F0005000E00000041005300530055004E0054004F000C00070000000E
      0020001E00320000000F000117000111000118000112000113000115000E0000
      0041005300530055004E0054004F001F0032000000FEFF0B0400100000004500
      4D00410049004C0055005300550005001000000045004D00410049004C005500
      530055000C00080000000E0020001E00140000000F0001170001110001180001
      12000113000115001000000045004D00410049004C005500530055001F001400
      0000FEFF0B04001200000043004C00490045004E005400450049004400050012
      00000043004C00490045004E0054004500490044000C00090000000E001A000F
      000117000111000118000112000113000115001200000043004C00490045004E
      005400450049004400FEFF0B04000C00000050004500520053004F004E000500
      0C00000050004500520053004F004E000C000A0000000E0020001E0032000000
      0F000117000111000118000112000113000115000C0000005000450052005300
      4F004E001F0032000000FEFF0B04000C00000045004D00410049004C00530005
      000C00000045004D00410049004C0053000C000B0000000E0020001E00320000
      000F000117000111000118000112000113000115000C00000045004D00410049
      004C0053001F0032000000FEFF0B040012000000430041005400450047004F00
      520049004100050012000000430041005400450047004F005200490041000C00
      0C0000000E0020001E00140000000F0001170001110001180001120001130001
      150012000000430041005400450047004F005200490041001F0014000000FEFF
      0B04000C00000041004E00450058004F00530005000C00000041004E00450058
      004F0053000C000D0000000E001B001700011C00011100011800011200011300
      0115000C00000041004E00450058004F005300FEFEFF21FEFF22FEFF23FEFEFE
      FF24FEFF25260052000000FF27FEFEFE0E004D0061006E006100670065007200
      1E00550070006400610074006500730052006500670069007300740072007900
      12005400610062006C0065004C006900730074000A005400610062006C006500
      08004E0061006D006500140053006F0075007200630065004E0061006D006500
      0A0054006100620049004400240045006E0066006F0072006300650043006F00
      6E00730074007200610069006E00740073001E004D0069006E0069006D007500
      6D0043006100700061006300690074007900180043006800650063006B004E00
      6F0074004E0075006C006C00140043006F006C0075006D006E004C0069007300
      74000C0043006F006C0075006D006E00100053006F0075007200630065004900
      44000E006400740049006E007400360034001000440061007400610054007900
      700065001400530065006100720063006800610062006C0065000E0041007500
      74006F0049006E0063000800420061007300650012004F0049006E0055007000
      640061007400650010004F0049006E005700680065007200650020004F004100
      660074006500720049006E0073004300680061006E006700650064001A004F00
      72006900670069006E0043006F006C004E0061006D0065000C00640074004400
      610074006500120041006C006C006F0077004E0075006C006C0014004F004100
      6C006C006F0077004E0075006C006C000C0064007400540069006D0065000E00
      6400740049006E0074003300320014006400740057006900640065004D006500
      6D006F00100042006C006F006200440061007400610018006400740041006E00
      7300690053007400720069006E0067000800530069007A006500140053006F00
      7500720063006500530069007A00650018006400740057006900640065005300
      7400720069006E0067001C0043006F006E00730074007200610069006E007400
      4C00690073007400100056006900650077004C006900730074000E0052006F00
      77004C006900730074001800520065006C006100740069006F006E004C006900
      730074001C0055007000640061007400650073004A006F00750072006E006100
      6C001200530061007600650050006F0069006E0074000E004300680061006E00
      670065007300}
    object cEnviosENVIOID: TLargeintField
      AutoGenerateValue = arAutoInc
      DisplayWidth = 15
      FieldName = 'ENVIOID'
      Required = True
    end
    object cEnviosDATA: TDateField
      DisplayWidth = 10
      FieldName = 'DATA'
    end
    object cEnviosHORA: TTimeField
      DisplayWidth = 10
      FieldName = 'HORA'
    end
    object cEnviosUSUARIO: TIntegerField
      DisplayWidth = 10
      FieldName = 'USUARIO'
    end
    object cEnviosCORPOEMAIL: TWideMemoField
      DisplayWidth = 10
      FieldName = 'CORPOEMAIL'
      BlobType = ftWideMemo
    end
    object cEnviosENVIADOS: TStringField
      DisplayWidth = 1
      FieldName = 'ENVIADOS'
      FixedChar = True
      Size = 1
    end
    object cEnviosASSUNTO: TWideStringField
      DisplayWidth = 50
      FieldName = 'ASSUNTO'
      Size = 50
    end
    object cEnviosEMAILUSU: TWideStringField
      DisplayWidth = 20
      FieldName = 'EMAILUSU'
    end
    object cEnviosCLIENTEID: TIntegerField
      DisplayWidth = 10
      FieldName = 'CLIENTEID'
    end
    object cEnviosPERSON: TWideStringField
      DisplayWidth = 50
      FieldName = 'PERSON'
      Size = 50
    end
    object cEnviosEMAILS: TWideStringField
      DisplayWidth = 50
      FieldName = 'EMAILS'
      Size = 50
    end
    object cEnviosCATEGORIA: TWideStringField
      DisplayWidth = 20
      FieldName = 'CATEGORIA'
    end
    object cEnviosANEXOS: TWideMemoField
      DisplayWidth = 10
      FieldName = 'ANEXOS'
      BlobType = ftWideMemo
    end
  end
  object UniAnexos: TUniQuery
    SQLInsert.Strings = (
      'INSERT INTO ANEXOS'
      '  (ANEXOS)'
      'VALUES'
      '  (:ANEXOS)'
      'SET :ID_ANEXO = SCOPE_IDENTITY()')
    SQLDelete.Strings = (
      'DELETE FROM ANEXOS'
      'WHERE'
      '  ID_ANEXO = :Old_ID_ANEXO')
    SQLUpdate.Strings = (
      'UPDATE ANEXOS'
      'SET'
      '  ANEXOS = :ANEXOS'
      'WHERE'
      '  ID_ANEXO = :Old_ID_ANEXO')
    SQLLock.Strings = (
      'SELECT * FROM ANEXOS'
      'WITH (UPDLOCK, ROWLOCK, HOLDLOCK)'
      'WHERE'
      '  ID_ANEXO = :Old_ID_ANEXO')
    SQLRefresh.Strings = (
      'SELECT ANEXOS FROM ANEXOS'
      'WHERE'
      '  ID_ANEXO = :ID_ANEXO')
    SQLRecCount.Strings = (
      'SET :PCOUNT = (SELECT COUNT(*) FROM ANEXOS'
      ')')
    Connection = SQLServer
    SQL.Strings = (
      'SELECT * FROM ANEXOS'
      'WHERE ID_ANEXO=-1')
    Left = 360
    Top = 192
    object UniAnexosID_ANEXO: TLargeintField
      FieldName = 'ID_ANEXO'
      ReadOnly = True
      Required = True
    end
    object UniAnexosANEXOS: TWideStringField
      FieldName = 'ANEXOS'
      Size = 4000
    end
  end
  object mAnexos: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 360
    Top = 248
    object mAnexosPATH: TStringField
      DisplayLabel = 'Arquivos'
      DisplayWidth = 500
      FieldName = 'PATH'
      Size = 500
    end
  end
  object UniUltimoAnexo: TUniQuery
    SQLDelete.Strings = (
      'DELETE FROM ANEXOS'
      'WHERE'
      '  ID_ANEXO = :Old_ID_ANEXO')
    SQLLock.Strings = (
      'SELECT * FROM ANEXOS'
      'WITH (UPDLOCK, ROWLOCK, HOLDLOCK)'
      'WHERE'
      '  ID_ANEXO = :Old_ID_ANEXO')
    SQLRecCount.Strings = (
      'SET :PCOUNT = (SELECT COUNT(*) FROM ANEXOS'
      ')')
    Connection = SQLServer
    SQL.Strings = (
      'SELECT TOP(1) ID_ANEXO FROM ANEXOS ORDER BY 1 DESC')
    Left = 248
    Top = 240
    object UniUltimoAnexoID_ANEXO: TLargeintField
      FieldName = 'ID_ANEXO'
      ReadOnly = True
      Required = True
    end
  end
end
