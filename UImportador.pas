﻿unit UImportador;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, vcl.wwdatsrc, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, IBX.IBQuery, IBX.IBCustomDataSet, vcl.wwdbigrd,
  vcl.wwdbgrid, Vcl.StdCtrls, Vcl.Grids;

type
  TFImportador = class(TForm)
    LbSuporte: TLabel;
    StringGrid: TStringGrid;
    Button1: TButton;
    btApagarCLI: TButton;
    BtGravarCLI: TButton;
    wwDBGrid1: TwwDBGrid;
    OpenDialog1: TOpenDialog;
    Produtos: TIBDataSet;
    ProdutosPRODUTO: TIBStringField;
    ProdutosREFERENCIA: TIBStringField;
    ProdutosEQUIVALENTE: TIBStringField;
    ProdutosLOCAL: TIBStringField;
    ProdutosNOME: TIBStringField;
    ProdutosMARCA: TIBStringField;
    ProdutosAPLICACAO: TIBStringField;
    ProdutosGRUPO: TIntegerField;
    ProdutosFORNECEDOR: TIntegerField;
    ProdutosFABRICANTE: TIntegerField;
    ProdutosQTDATUAL: TIBBCDField;
    ProdutosQTDMINIMA: TIBBCDField;
    ProdutosCUSTO: TIBBCDField;
    ProdutosCUSTOMEDIO: TIBBCDField;
    ProdutosPRECO: TIBBCDField;
    ProdutosDT_COMPRA: TDateField;
    ProdutosDT_VENDA: TDateField;
    ProdutosCOMISSAO: TIBBCDField;
    ProdutosUNIDADE: TIBStringField;
    ProdutosTRIBUTO: TIBStringField;
    ProdutosDESCONTO: TIBBCDField;
    ProdutosSERVICO: TIBStringField;
    ProdutosAPLICACOES: TWideMemoField;
    ProdutosATIVO: TIBStringField;
    ProdutosINSTRUCOES: TWideMemoField;
    ProdutosMARGEM_SEGURANCA: TIBBCDField;
    ProdutosQTD_VENDA: TIBBCDField;
    ProdutosCURVA: TIBStringField;
    ProdutosUSAR_MARGEM_CURVA: TIBStringField;
    ProdutosMARGEM_CURVA: TIntegerField;
    ProdutosNCM: TIBStringField;
    ProdutosETIQUETA: TIBStringField;
    ProdutosCOMPRA: TIBStringField;
    ProdutosCST: TIBStringField;
    ProdutosCSON: TIBStringField;
    ProdutosICMS: TIntegerField;
    ProdutosIPI: TIntegerField;
    ProdutosST: TIntegerField;
    ProdutosCURVA_ESTOQUE: TIBStringField;
    ProdutosQTD_MAXIMA: TIntegerField;
    ProdutosQTD_SEGURANCA: TIBBCDField;
    ProdutosQTD_GARANTIA: TIntegerField;
    ProdutosOBSERVACAO: TWideMemoField;
    ProdutosPRODUTO_FINAL: TIBStringField;
    ProdutosGERA_COMISSAO: TIBStringField;
    ProdutosBAIXAR_ESTOQUE: TIBStringField;
    ProdutosIPPT: TIBStringField;
    ProdutosINGREDIENTE: TIBStringField;
    ProdutosHASH: TBlobField;
    ProdutosMESCLAR: TIBStringField;
    ProdutosID_SETOR: TIntegerField;
    ProdutosCOR: TIntegerField;
    ProdutosTAMANHO: TIntegerField;
    ProdutosCOLECAO: TIntegerField;
    ProdutosBARRAS: TIBStringField;
    ProdutosPOSSUI_SABOR: TIBStringField;
    ProdutosMAIS_PEDIDO: TIBStringField;
    ProdutosSEGMENTACAO: TIBStringField;
    ProdutosPOSSUI_TAMANHO: TIBStringField;
    ProdutosIPICP: TIntegerField;
    ProdutosICMSCP: TIntegerField;
    ProdutosSTCP: TIntegerField;
    ProdutosCT_VARIAVEIS: TIntegerField;
    ProdutosLUCROLIQUIDO: TIBBCDField;
    ProdutosPRIORIDADE: TIntegerField;
    ProdutosHORA_LIMITE: TTimeField;
    ProdutosCASA_DECIMAL: TIntegerField;
    ProdutosIAT: TIBStringField;
    ProdutosIMPORTADO: TIntegerField;
    ProdutosPRODUZIDO: TIBStringField;
    ProdutosID_COMBO: TIntegerField;
    ProdutosDOUBLE_ITEM: TIBStringField;
    ProdutosCFOP: TIBStringField;
    ProdutosREPLICADO: TIBStringField;
    ProdutosID_TOPIN: TIntegerField;
    ProdutosPONTO_CARNE: TIBStringField;
    ProdutosCODIGO_IFOOD: TIBStringField;
    ProdutosPODE_COMPRAR: TIBStringField;
    ProdutosMARGEM: TFloatField;
    ProdutosDOM: TIBStringField;
    ProdutosSEG: TIBStringField;
    ProdutosTER: TIBStringField;
    ProdutosQUA: TIBStringField;
    ProdutosQUI: TIBStringField;
    ProdutosSEX: TIBStringField;
    ProdutosSAB: TIBStringField;
    ProdutosINICIO_VENDA: TTimeField;
    ProdutosFIM_VENDA: TTimeField;
    BuscaCodGrupo: TIBQuery;
    BuscaCodGrupoGRUPO: TIntegerField;
    GRUPOS: TIBDataSet;
    GRUPOSGRUPO: TIntegerField;
    GRUPOSNOME: TIBStringField;
    GRUPOSESTOQUE: TIBStringField;
    GRUPOSID_IMPRESSORA: TIntegerField;
    GRUPOSILHA: TIntegerField;
    GRUPOSID_ESTOQUES: TIntegerField;
    GRUPOSID_FAMILIA: TIntegerField;
    CLI: TIBDataSet;
    CLICLIENTE: TIntegerField;
    CLINOME: TIBStringField;
    CLIENDERECO: TIBStringField;
    CLIBAIRRO: TIBStringField;
    CLICIDADE: TIBStringField;
    CLIUF: TIBStringField;
    CLICEP: TIBStringField;
    CLICIC: TIBStringField;
    CLIDT_CADASTRO: TDateField;
    CLIDT_NASCIMENTO: TDateField;
    CLIDT_ULT_COMPRA: TDateField;
    CLIINSC_IDENT: TIBStringField;
    CLIDDD: TIBStringField;
    CLITELEFONE: TIBStringField;
    CLIFAX: TIBStringField;
    CLIEMAIL: TIBStringField;
    CLIBLOQUEADO: TIBStringField;
    CLIMOTIVO: TIBStringField;
    CLIP1_DE: TIntegerField;
    CLIP1_ATE: TIntegerField;
    CLIP1_VENCIMENTO: TIntegerField;
    CLIP2_DE: TIntegerField;
    CLIP2_ATE: TIntegerField;
    CLIP2_VENCIMENTO: TIntegerField;
    CLIUSARLIMITE: TIBStringField;
    CLILIMITE: TIBBCDField;
    CLIDESCONTO: TIBStringField;
    CLIOBS: TWideMemoField;
    CLIVALOR_DESCONTO: TIntegerField;
    CLIECF: TIBStringField;
    CLIBOLETO: TIBStringField;
    CLICARTEIRA: TIBStringField;
    CLIGRUPO: TIntegerField;
    CLIROTA: TIntegerField;
    CLITAXA_ENTREGA: TFloatField;
    CLICLASSIFICACAO: TIntegerField;
    CLIFRETE_POR_CONTA: TIBStringField;
    CLIFRETE_TIPO: TIBStringField;
    CLIACRESCIMO_NOTA: TIntegerField;
    CLIVENDEDOR: TIntegerField;
    CLIOS: TIBStringField;
    CLITIPO_FAT: TIBStringField;
    CLIMESSAGEM_FINANCEIRO: TIBStringField;
    CLIENDERECO_NUM: TIBStringField;
    CLIENDERECO_CPL: TIBStringField;
    CLIENDERECO_COD_MUN: TIntegerField;
    CLIENDERECO_COD_UF: TIntegerField;
    CLITABELA: TIBStringField;
    CLIENDERECO_REF: TWideMemoField;
    CLIENDERECO_REF_COB: TBlobField;
    CLIBAI_COB: TIBStringField;
    CLICID_COB: TIBStringField;
    CLIUF_COB: TIBStringField;
    CLIENDERECO_COD_UF_COB: TIntegerField;
    CLIENDERECO_COD_MUN_COB: TIntegerField;
    CLIEND_COB: TIBStringField;
    CLICEP_COB: TIBStringField;
    CLIUSUARIO: TIBStringField;
    CLISEXO: TIBStringField;
    CLIPALAVRA: TIBStringField;
    CLIMENSALIDADE: TFloatField;
    CLICFOP: TIBStringField;
    CLICONTATO: TIBStringField;
    CLICPF_DONO: TIBStringField;
    CLIELISEU: TIBStringField;
    CLIDATA_ELISEU: TDateField;
    CLIEMAIL2: TIBStringField;
    CLIEMAIL3: TIBStringField;
    CLIRAZAO_SOCIAL: TIBStringField;
    CLISERVENTIA: TIntegerField;
    CLIID_INSC: TIntegerField;
    CLICARTORIO: TIBStringField;
    CLIPAGAMENTO_PREFERENCIAL: TIBStringField;
    CLIPERIODICIDADE_PREFERENCIAL: TIBStringField;
    CLIENVIA_CARNE_EMAIL: TIBStringField;
    CLIENDERECO_CPL_COB: TIBStringField;
    CLIENDERECO_NUM_COB: TIBStringField;
    CLITERMO_ADESAO: TIBStringField;
    FNC: TIBDataSet;
    FNCFORNECEDOR: TIntegerField;
    FNCNOME: TIBStringField;
    FNCCONTATO: TIBStringField;
    FNCENDERECO: TIBStringField;
    FNCBAIRRO: TIBStringField;
    FNCCIDADE: TIBStringField;
    FNCUF: TIBStringField;
    FNCCEP: TIBStringField;
    FNCCGC: TIBStringField;
    FNCINSCRICAO: TIBStringField;
    FNCTELEFONE: TIBStringField;
    FNCOBS: TWideMemoField;
    FNCENDERECO_NUM: TIBStringField;
    FNCENDERECO_CPL: TIBStringField;
    FNCENDERECO_COD_MUN: TIntegerField;
    FNCENDERECO_COD_UF: TIntegerField;
    FNCTIPO: TIBStringField;
    FNCTRANS_ANTT: TIBStringField;
    FNCRAZAO_SOCIAL: TIBStringField;
    FNCTAXA: TFloatField;
    FNCEMAIL: TIBStringField;
    FNCATIVO: TIBStringField;
    TAMANHO: TIBDataSet;
    TAMANHOID_TAMANHO: TIntegerField;
    TAMANHOTAMANHO: TIBStringField;
    TAMANHOPERC_PROPORCAO: TIBBCDField;
    BuscaCodTamanho: TIBQuery;
    BuscaCodTamanhoID_TAMANHO: TIntegerField;
    cdsReceitas: TFDMemTable;
    cdsReceitasDATA_VENDA: TDateField;
    cdsReceitasHORA_VENDA: TTimeField;
    cdsReceitasTIPO_PAGAMENTO: TStringField;
    cdsReceitasID_OPERADORA: TStringField;
    cdsReceitasBANDEIRA: TStringField;
    cdsReceitasVALOR_BRUTO: TFloatField;
    cdsReceitasVALOR_LIQUIDO: TFloatField;
    cdsReceitasN_CARTAO: TStringField;
    cdsReceitasOBS: TStringField;
    dsReceitas: TwwDataSource;
    procedure Button1Click(Sender: TObject);
    function XlsToStringGrid(AGrid: TStringGrid; AXLSFile: string): Boolean;
    procedure btApagarCLIClick(Sender: TObject);
    procedure BtGravarCLIClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FImportador: TFImportador;

implementation

Uses UDM, System.Win.ComObj, UGeralPAF;

{$R *.dfm}

procedure TFImportador.btApagarCLIClick(Sender: TObject);
begin

    dm.CLIENTES.Close;
    dm.CLIENTES.SQL.Clear;
    dm.CLIENTES.SQL.Add('Delete FROM ClIENTE where CLIENTEID <> 1');
//    dm.CLIENTES.SQL.Add('ORDER BY CAST(CLIENTEID AS INTEGER)');
    dm.CLIENTES.Open;

end;

procedure TFImportador.BtGravarCLIClick(Sender: TObject);
var
  nLinha :integer;
begin
  dm.CLIENTES.Close;
  dm.CLIENTES.Open;
  For nLinha := 1 to StringGrid.RowCount - 1 do
  begin

      if not (gr.PegarNumeroTexto( copy((StringGrid.Cells[8,nLinha]),0,25))= '')  or not (copy((StringGrid.Cells[1,nLinha]),0,39)= '') then
      begin
          dm.CLIENTES.Append;

          dm.CLIENTESCATEGORIA.AsString          :=  copy((StringGrid.Cells[0,nLinha]),0,19); {Lista 0}
          dm.CLIENTESNOME.AsString               :=  copy((StringGrid.Cells[1,nLinha]),0,39); {nome 1}
          dm.CLIENTESEMAIL.AsString              :=  copy((StringGrid.Cells[2,nLinha]),0,89); {email 2}
          dm.CLIENTESPERSON.AsString             :=  copy((StringGrid.Cells[3,nLinha]),0,50); {Person 3}
          dm.CLIENTESTELEFONE.AsString           :=  '';  {Tel 4}
          dm.CLIENTESOBS.AsString                :=  '';  {Obs 5}

          dm.CLIENTES.Post;


      end;
  end;
  Dm.CLIENTES.close;

  ShowMessage('acho que foi..rs');
end;

procedure TFImportador.Button1Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
	  XlsToStringGrid(StringGrid,OpenDialog1.FileName);
end;

function TFImportador.XlsToStringGrid(AGrid: TStringGrid; AXLSFile: string): Boolean;
const
	xlCellTypeLastCell = $0000000B;
var
	XLApp, Sheet: OLEVariant;
	RangeMatrix: Variant;
	x, y, k, r: Integer;
begin
  Result:=False;
  //Cria Excel- OLE Object
  XLApp:=CreateOleObject('Excel.Application');

  try
    //Esconde Excel
    XLApp.Visible:=False;
    //Abre o Workbook
    XLApp.Workbooks.Open(AXLSFile);
    Sheet:=XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[1];
    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    //Pegar o n�mero da �ltima linha
    x:=XLApp.ActiveCell.Row;
    //Pegar o n�mero da �ltima coluna
    y:=XLApp.ActiveCell.Column;
    //Seta Stringgrid linha e coluna
    AGrid.RowCount:=x;
    AGrid.ColCount:=y;
    //Associaca a variant WorkSheet com a variant do Delphi
    RangeMatrix:=XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;
    //Cria o loop para listar os registros no TStringGrid
    k:=1;
    repeat
      for r:=1 to y do
        AGrid.Cells[(r - 1),(k - 1)]:=RangeMatrix[K, R];
      Inc(k,1);
    until k > x;
    RangeMatrix:=Unassigned;
    finally
    //Fecha o Excel
    if not VarIsEmpty(XLApp) then
    begin
      XLApp.Quit;
      XLAPP:=Unassigned;
      Sheet:=Unassigned;
      Result:=True;
    end;
  end;


end;

end.
