object Disparador: TDisparador
  Left = 0
  Top = 0
  BorderStyle = bsNone
  BorderWidth = 4
  Caption = 'Disparador'
  ClientHeight = 467
  ClientWidth = 788
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PDisparador: TPanel
    Left = 0
    Top = 0
    Width = 788
    Height = 467
    Align = alClient
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 786
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      Color = 7690564
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -32
      Font.Name = 'Segoe UI Light'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      object LbEmail: TLabel
        Left = 291
        Top = 8
        Width = 155
        Height = 25
        Cursor = crHandPoint
        Alignment = taCenter
        BiDiMode = bdLeftToRight
        Caption = 'Validador de emails'
        Color = clRed
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -19
        Font.Name = 'Segoe UI Light'
        Font.Style = []
        ParentBiDiMode = False
        ParentColor = False
        ParentFont = False
      end
      object EdSetf: TEdit
        Left = 801
        Top = 14
        Width = 26
        Height = 26
        BevelInner = bvNone
        BevelOuter = bvNone
        BorderStyle = bsNone
        Color = 12673280
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 12673280
        Font.Height = -16
        Font.Name = 'Segoe UI'
        Font.Style = []
        MaxLength = 6
        ParentFont = False
        TabOrder = 0
        Text = '12'
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 786
        Height = 9
        Align = alTop
        BevelOuter = bvNone
        Color = 7690564
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -32
        Font.Name = 'Segoe UI Light'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        object Label1: TLabel
          Left = 291
          Top = 8
          Width = 155
          Height = 25
          Cursor = crHandPoint
          Alignment = taCenter
          BiDiMode = bdLeftToRight
          Caption = 'Validador de emails'
          Color = clRed
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Segoe UI Light'
          Font.Style = []
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
        end
        object Edit1: TEdit
          Left = 801
          Top = 14
          Width = 26
          Height = 26
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          Color = 12673280
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 12673280
          Font.Height = -16
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 6
          ParentFont = False
          TabOrder = 0
          Text = '12'
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 786
        Height = 32
        Align = alBottom
        BevelOuter = bvNone
        Color = 7690564
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -32
        Font.Name = 'Segoe UI Light'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 2
        object Label2: TLabel
          Left = 198
          Top = 5
          Width = 335
          Height = 25
          Cursor = crHandPoint
          Alignment = taCenter
          BiDiMode = bdLeftToRight
          Caption = 'Selecione os e-mail que n'#227'o deseja enviar'
          Color = clRed
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Segoe UI Light'
          Font.Style = []
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
        end
      end
    end
    object GridList_Cliente: TDBGrid
      Left = 789
      Top = 66
      Width = 66
      Height = 360
      BorderStyle = bsNone
      DataSource = UdsCLientes
      GradientEndColor = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI Semibold'
      Font.Style = []
      Options = [dgTitles, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnCellClick = GridList_ClienteCellClick
      OnDrawColumnCell = GridList_ClienteDrawColumnCell
      OnDblClick = GridList_ClienteDblClick
      OnTitleClick = GridList_ClienteTitleClick
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CATEGORIA'
          Title.Alignment = taCenter
          Title.Caption = 'GROUP LIST'
          Width = 182
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'NOME'
          Title.Alignment = taCenter
          Title.Caption = 'COMPANY NAME'
          Width = 324
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'EMAIL'
          Title.Alignment = taCenter
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SELECIONAR'
          Title.Caption = 'RETIRAR'
          Width = 78
          Visible = True
        end>
    end
    object Panel2: TPanel
      Left = 1
      Top = 426
      Width = 786
      Height = 40
      Align = alBottom
      BevelOuter = bvNone
      Color = 7690564
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -32
      Font.Name = 'Segoe UI Light'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 2
      object Panel11: TPanel
        Left = 560
        Top = 9
        Width = 97
        Height = 21
        Cursor = crHandPoint
        BevelOuter = bvNone
        Caption = 'Enviar'
        Color = 12673280
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'FontAwesome'
        Font.Pitch = fpVariable
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        OnClick = Panel11Click
      end
      object Panel3: TPanel
        Left = 671
        Top = 9
        Width = 97
        Height = 21
        Cursor = crHandPoint
        BevelOuter = bvNone
        Caption = 'Cancelar'
        Color = 12673280
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'FontAwesome'
        Font.Pitch = fpVariable
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        OnClick = Panel3Click
      end
    end
    object Panel6: TPanel
      Left = 1
      Top = 33
      Width = 786
      Height = 33
      Align = alTop
      BevelOuter = bvNone
      Color = 7690564
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -32
      Font.Name = 'Segoe UI Light'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 3
      object Label18: TLabel
        Left = 534
        Top = 4
        Width = 20
        Height = 21
        Alignment = taCenter
        Caption = #61442
        Color = clSilver
        Font.Charset = ANSI_CHARSET
        Font.Color = clGray
        Font.Height = -21
        Font.Name = 'FontAwesome'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        OnClick = Label18Click
      end
      object edNome: TEdit
        Left = 224
        Top = 4
        Width = 302
        Height = 21
        Cursor = crIBeam
        Margins.Left = 1
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 1
        CharCase = ecUpperCase
        Ctl3D = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = cl3DDkShadow
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Pitch = fpVariable
        Font.Style = []
        MaxLength = 50
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Text = 'LOCALIZAR PELO NOME'
        OnEnter = edNomeEnter
        OnExit = edNomeExit
        OnKeyPress = edNomeKeyPress
      end
      object EdGroupList: TEdit
        Left = 49
        Top = 4
        Width = 172
        Height = 21
        Cursor = crIBeam
        Margins.Left = 1
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 1
        CharCase = ecUpperCase
        Ctl3D = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = cl3DDkShadow
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Pitch = fpVariable
        Font.Style = []
        MaxLength = 50
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        Text = 'LOCALIZAR GROUP LIST'
        OnEnter = EdGroupListEnter
        OnExit = EdGroupListExit
        OnKeyPress = EdGroupListKeyPress
      end
    end
    object wwDBGrid1: TwwDBGrid
      Left = 1
      Top = 66
      Width = 786
      Height = 360
      ControlType.Strings = (
        'XCHECK;CheckBox;1;0'
        'SELECIONAR;CheckBox;True;False'
        'XCHEC;CheckBox;1;0')
      Selected.Strings = (
        'XCHECK'#9'8'#9'RETIRAR'#9'F'
        'CATEGORIA'#9'28'#9'CATEGORIA'#9'F'
        'NOME'#9'49'#9'NOME'#9'F'
        'EMAIL'#9'40'#9'EMAIL'#9'F')
      IniAttributes.Delimiter = ';;'
      IniAttributes.UnicodeIniFile = False
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      BorderStyle = bsNone
      DataSource = UdsCLientes
      Options = [dgTitles, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgTrailingEllipsis, dgProportionalColResize]
      TabOrder = 4
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      TitleLines = 1
      TitleButtons = False
      UseTFields = False
      OnMouseDown = wwDBGrid1MouseDown
      OnMouseMove = wwDBGrid1MouseMove
    end
  end
  object UdsCLientes: TUniDataSource
    DataSet = dm.UniClientes
    Left = 714
    Top = 296
  end
  object UniDataSource1: TUniDataSource
    DataSet = dm.CLIENTES
    Left = 714
    Top = 232
  end
end
