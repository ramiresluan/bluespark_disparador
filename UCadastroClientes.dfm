object FCadastroClientes: TFCadastroClientes
  Left = 356
  Top = 166
  BorderIcons = []
  BorderStyle = bsNone
  BorderWidth = 4
  Caption = 'CADASTRO DE CLIENTE'
  ClientHeight = 509
  ClientWidth = 554
  Color = clCream
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object P1: TPanel
    Left = 0
    Top = 40
    Width = 554
    Height = 469
    Align = alClient
    BevelOuter = bvNone
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Arial'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Label2: TLabel
      Left = 11
      Top = 24
      Width = 41
      Height = 20
      Caption = 'Nome'
      Font.Charset = ANSI_CHARSET
      Font.Color = 7098166
      Font.Height = -15
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 11
      Top = 209
      Width = 146
      Height = 17
      Caption = 'PERSON IN CHARGE'
      Font.Charset = ANSI_CHARSET
      Font.Color = 7098166
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 11
      Top = 255
      Width = 75
      Height = 20
      Caption = '1'#186' Telefone'
      Font.Charset = ANSI_CHARSET
      Font.Color = 7098166
      Font.Height = -15
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label14: TLabel
      Left = 11
      Top = 87
      Width = 43
      Height = 20
      Caption = 'E-mail'
      Font.Charset = ANSI_CHARSET
      Font.Color = 7098166
      Font.Height = -15
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label15: TLabel
      Left = 11
      Top = 309
      Width = 78
      Height = 20
      Caption = 'Observa'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = 7098166
      Font.Height = -15
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 11
      Top = 143
      Width = 79
      Height = 20
      Caption = 'GROUP LIST'
      Font.Charset = ANSI_CHARSET
      Font.Color = 7098166
      Font.Height = -15
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edNome: TDBEdit
      Left = 163
      Top = 22
      Width = 388
      Height = 26
      AutoSize = False
      CharCase = ecUpperCase
      Ctl3D = False
      DataField = 'NOME'
      DataSource = UdSClientes
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
    end
    object edTelefone1: TDBEdit
      Left = 163
      Top = 255
      Width = 143
      Height = 25
      AutoSize = False
      Ctl3D = False
      DataField = 'TELEFONE'
      DataSource = UdSClientes
      ParentCtl3D = False
      TabOrder = 2
      OnKeyPress = edTelefone1KeyPress
    end
    object edEmail: TDBEdit
      Left = 163
      Top = 85
      Width = 388
      Height = 25
      AutoSize = False
      CharCase = ecLowerCase
      Ctl3D = False
      DataField = 'EMAIL'
      DataSource = UdSClientes
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Segoe UI'
      Font.Style = []
      MaxLength = 95
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
    end
    object MMObservacao: TDBMemo
      Left = 11
      Top = 335
      Width = 540
      Height = 71
      BevelInner = bvSpace
      BevelOuter = bvRaised
      BevelKind = bkFlat
      BorderStyle = bsNone
      DataField = 'OBS'
      DataSource = UdSClientes
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
    object lkGRUPO: TDBLookupComboBox
      Left = 163
      Top = 143
      Width = 388
      Height = 26
      BevelInner = bvSpace
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Segoe UI'
      Font.Style = []
      KeyField = 'LISTAID'
      ListField = 'CATEGORIA'
      ListSource = UdsCategoria
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
    end
    object btSalvar: TPanel
      Left = 287
      Top = 425
      Width = 129
      Height = 36
      Cursor = crHandPoint
      BevelOuter = bvNone
      Caption = '    Salvar'
      Color = 12673280
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 5
      OnClick = btSalvarClick
      object Label12: TLabel
        Left = 21
        Top = 8
        Width = 17
        Height = 18
        Caption = #61452
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -17
        Font.Name = 'FontAwesome'
        Font.Style = []
        ParentFont = False
      end
    end
    object btCancelar: TPanel
      Left = 422
      Top = 425
      Width = 129
      Height = 36
      Cursor = crHandPoint
      BevelOuter = bvNone
      Caption = '    Cancelar'
      Color = 12673280
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 6
      OnClick = btCancelarClick
      object Label13: TLabel
        Left = 18
        Top = 8
        Width = 13
        Height = 18
        Caption = #61453
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -17
        Font.Name = 'FontAwesome'
        Font.Style = []
        ParentFont = False
      end
    end
    object DBEdit1: TDBEdit
      Left = 163
      Top = 200
      Width = 388
      Height = 26
      AutoSize = False
      CharCase = ecUpperCase
      Ctl3D = False
      DataField = 'PERSON'
      DataSource = UdSClientes
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 7
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 0
    Width = 554
    Height = 40
    Align = alTop
    BevelOuter = bvNone
    Color = 7690564
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -32
    Font.Name = 'Segoe UI Light'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object LbAbrirCaixa: TLabel
      Left = 195
      Top = 7
      Width = 165
      Height = 25
      Cursor = crHandPoint
      Alignment = taCenter
      BiDiMode = bdLeftToRight
      Caption = 'Cadastro de Clientes'
      Color = clRed
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Segoe UI Light'
      Font.Style = []
      ParentBiDiMode = False
      ParentColor = False
      ParentFont = False
    end
    object EdSetf: TEdit
      Left = 801
      Top = 14
      Width = 26
      Height = 26
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = 12673280
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 12673280
      Font.Height = -16
      Font.Name = 'Segoe UI'
      Font.Style = []
      MaxLength = 6
      ParentFont = False
      TabOrder = 0
      Text = '12'
    end
  end
  object UdsCategoria: TUniDataSource
    DataSet = dm.CATEGORIA
    Left = 448
    Top = 304
  end
  object UdSClientes: TUniDataSource
    DataSet = dm.CLIENTES
    Left = 368
    Top = 304
  end
end
