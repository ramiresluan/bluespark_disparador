object FImportador: TFImportador
  Left = 0
  Top = 0
  Caption = 'FImportador'
  ClientHeight = 424
  ClientWidth = 810
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object LbSuporte: TLabel
    Left = 433
    Top = 28
    Width = 18
    Height = 21
    Cursor = crHandPoint
    Caption = #61530
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -21
    Font.Name = 'FontAwesome'
    Font.Style = []
    ParentFont = False
  end
  object StringGrid: TStringGrid
    Left = 0
    Top = 114
    Width = 810
    Height = 190
    Align = alBottom
    Color = clBtnHighlight
    TabOrder = 0
    ColWidths = (
      64
      64
      64
      64
      64)
    RowHeights = (
      24
      24
      24
      24
      24)
  end
  object Button1: TButton
    Left = 48
    Top = 16
    Width = 129
    Height = 33
    Caption = 'puxar Excel'
    TabOrder = 1
    OnClick = Button1Click
  end
  object btApagarCLI: TButton
    Left = 193
    Top = 16
    Width = 113
    Height = 33
    Caption = 'Apagar CLIENTES'
    TabOrder = 2
    OnClick = btApagarCLIClick
  end
  object BtGravarCLI: TButton
    Left = 312
    Top = 16
    Width = 105
    Height = 33
    Caption = 'Gravar na Clientes'
    TabOrder = 3
    OnClick = BtGravarCLIClick
  end
  object wwDBGrid1: TwwDBGrid
    Left = 0
    Top = 304
    Width = 810
    Height = 120
    Selected.Strings = (
      'DATA_VENDA'#9'10'#9'DATA_VENDA'#9#9
      'HORA_VENDA'#9'10'#9'HORA_VENDA'#9#9
      'TIPO_PAGAMENTO'#9'30'#9'TIPO_PAGAMENTO'#9#9
      'ID_OPERADORA'#9'30'#9'ID_OPERADORA'#9#9
      'BANDEIRA'#9'30'#9'BANDEIRA'#9#9
      'VALOR_BRUTO'#9'10'#9'VALOR_BRUTO'#9#9
      'VALOR_LIQUIDO'#9'10'#9'VALOR_LIQUIDO'#9#9
      'N_CARTAO'#9'30'#9'N_CARTAO'#9#9
      'OBS'#9'100'#9'OBS'#9#9)
    IniAttributes.Delimiter = ';;'
    IniAttributes.UnicodeIniFile = False
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alBottom
    DataSource = dsReceitas
    TabOrder = 4
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = False
  end
  object OpenDialog1: TOpenDialog
    Left = 427
    Top = 56
  end
  object Produtos: TIBDataSet
    BufferChunks = 1000
    CachedUpdates = False
    DeleteSQL.Strings = (
      'delete from EST'
      'where'
      '  PRODUTO = :OLD_PRODUTO')
    InsertSQL.Strings = (
      'insert into EST'
      
        '  (PRODUTO, REFERENCIA, EQUIVALENTE, LOCAL, NOME, MARCA, APLICAC' +
        'AO, GRUPO, '
      
        '   FORNECEDOR, FABRICANTE, QTDATUAL, QTDMINIMA, CUSTO, CUSTOMEDI' +
        'O, PRECO, '
      
        '   DT_COMPRA, DT_VENDA, COMISSAO, UNIDADE, TRIBUTO, DESCONTO, SE' +
        'RVICO, '
      
        '   APLICACOES, ATIVO, INSTRUCOES, MARGEM_SEGURANCA, QTD_VENDA, C' +
        'URVA, USAR_MARGEM_CURVA, '
      
        '   MARGEM_CURVA, NCM, ETIQUETA, COMPRA, CST, CSON, ICMS, IPI, ST' +
        ', CURVA_ESTOQUE, '
      
        '   QTD_MAXIMA, QTD_SEGURANCA, QTD_GARANTIA, OBSERVACAO, PRODUTO_' +
        'FINAL, '
      
        '   GERA_COMISSAO, BAIXAR_ESTOQUE, IPPT, INGREDIENTE, HASH, MESCL' +
        'AR, ID_SETOR, '
      
        '   COR, TAMANHO, COLECAO, BARRAS, POSSUI_SABOR, MAIS_PEDIDO, SEG' +
        'MENTACAO, '
      
        '   POSSUI_TAMANHO, IPICP, ICMSCP, STCP, CT_VARIAVEIS, PRIORIDADE' +
        ', HORA_LIMITE, '
      
        '   CASA_DECIMAL, IAT, IMPORTADO, PRODUZIDO, ID_COMBO, DOUBLE_ITE' +
        'M, CFOP, '
      
        '   REPLICADO, ID_TOPIN, PONTO_CARNE, CODIGO_IFOOD, PODE_COMPRAR,' +
        ' MARGEM, '
      '   DOM, SEG, TER, QUA, QUI, SEX, SAB, INICIO_VENDA, FIM_VENDA)'
      'values'
      
        '  (:PRODUTO, :REFERENCIA, :EQUIVALENTE, :LOCAL, :NOME, :MARCA, :' +
        'APLICACAO, '
      
        '   :GRUPO, :FORNECEDOR, :FABRICANTE, :QTDATUAL, :QTDMINIMA, :CUS' +
        'TO, :CUSTOMEDIO, '
      
        '   :PRECO, :DT_COMPRA, :DT_VENDA, :COMISSAO, :UNIDADE, :TRIBUTO,' +
        ' :DESCONTO, '
      
        '   :SERVICO, :APLICACOES, :ATIVO, :INSTRUCOES, :MARGEM_SEGURANCA' +
        ', :QTD_VENDA, '
      
        '   :CURVA, :USAR_MARGEM_CURVA, :MARGEM_CURVA, :NCM, :ETIQUETA, :' +
        'COMPRA, '
      
        '   :CST, :CSON, :ICMS, :IPI, :ST, :CURVA_ESTOQUE, :QTD_MAXIMA, :' +
        'QTD_SEGURANCA, '
      
        '   :QTD_GARANTIA, :OBSERVACAO, :PRODUTO_FINAL, :GERA_COMISSAO, :' +
        'BAIXAR_ESTOQUE, '
      
        '   :IPPT, :INGREDIENTE, :HASH, :MESCLAR, :ID_SETOR, :COR, :TAMAN' +
        'HO, :COLECAO, '
      
        '   :BARRAS, :POSSUI_SABOR, :MAIS_PEDIDO, :SEGMENTACAO, :POSSUI_T' +
        'AMANHO, '
      
        '   :IPICP, :ICMSCP, :STCP, :CT_VARIAVEIS, :PRIORIDADE, :HORA_LIM' +
        'ITE, :CASA_DECIMAL, '
      
        '   :IAT, :IMPORTADO, :PRODUZIDO, :ID_COMBO, :DOUBLE_ITEM, :CFOP,' +
        ' :REPLICADO, '
      
        '   :ID_TOPIN, :PONTO_CARNE, :CODIGO_IFOOD, :PODE_COMPRAR, :MARGE' +
        'M, :DOM, '
      
        '   :SEG, :TER, :QUA, :QUI, :SEX, :SAB, :INICIO_VENDA, :FIM_VENDA' +
        ')')
    RefreshSQL.Strings = (
      'Select '
      '  PRODUTO,'
      '  REFERENCIA,'
      '  EQUIVALENTE,'
      '  LOCAL,'
      '  NOME,'
      '  MARCA,'
      '  APLICACAO,'
      '  GRUPO,'
      '  FORNECEDOR,'
      '  FABRICANTE,'
      '  QTDATUAL,'
      '  QTDMINIMA,'
      '  CUSTO,'
      '  CUSTOMEDIO,'
      '  PRECO,'
      '  DT_COMPRA,'
      '  DT_VENDA,'
      '  COMISSAO,'
      '  UNIDADE,'
      '  TRIBUTO,'
      '  DESCONTO,'
      '  SERVICO,'
      '  APLICACOES,'
      '  ATIVO,'
      '  INSTRUCOES,'
      '  MARGEM_SEGURANCA,'
      '  QTD_VENDA,'
      '  CURVA,'
      '  USAR_MARGEM_CURVA,'
      '  MARGEM_CURVA,'
      '  NCM,'
      '  ETIQUETA,'
      '  COMPRA,'
      '  CST,'
      '  CSON,'
      '  ICMS,'
      '  IPI,'
      '  ST,'
      '  CURVA_ESTOQUE,'
      '  QTD_MAXIMA,'
      '  QTD_SEGURANCA,'
      '  QTD_GARANTIA,'
      '  OBSERVACAO,'
      '  PRODUTO_FINAL,'
      '  GERA_COMISSAO,'
      '  BAIXAR_ESTOQUE,'
      '  IPPT,'
      '  INGREDIENTE,'
      '  HASH,'
      '  MESCLAR,'
      '  ID_SETOR,'
      '  COR,'
      '  TAMANHO,'
      '  COLECAO,'
      '  BARRAS,'
      '  POSSUI_SABOR,'
      '  MAIS_PEDIDO,'
      '  SEGMENTACAO,'
      '  POSSUI_TAMANHO,'
      '  IPICP,'
      '  ICMSCP,'
      '  STCP,'
      '  CT_VARIAVEIS,'
      '  LUCROLIQUIDO,'
      '  PRIORIDADE,'
      '  HORA_LIMITE,'
      '  CASA_DECIMAL,'
      '  IAT,'
      '  IMPORTADO,'
      '  PRODUZIDO,'
      '  ID_COMBO,'
      '  DOUBLE_ITEM,'
      '  CFOP,'
      '  REPLICADO,'
      '  ID_TOPIN,'
      '  PONTO_CARNE,'
      '  CODIGO_IFOOD,'
      '  PODE_COMPRAR,'
      '  MARGEM,'
      '  DOM,'
      '  SEG,'
      '  TER,'
      '  QUA,'
      '  QUI,'
      '  SEX,'
      '  SAB,'
      '  INICIO_VENDA,'
      '  FIM_VENDA'
      'from EST '
      'where'
      '  PRODUTO = :PRODUTO')
    SelectSQL.Strings = (
      'select * from EST')
    ModifySQL.Strings = (
      'update EST'
      'set'
      '  PRODUTO = :PRODUTO,'
      '  REFERENCIA = :REFERENCIA,'
      '  EQUIVALENTE = :EQUIVALENTE,'
      '  LOCAL = :LOCAL,'
      '  NOME = :NOME,'
      '  MARCA = :MARCA,'
      '  APLICACAO = :APLICACAO,'
      '  GRUPO = :GRUPO,'
      '  FORNECEDOR = :FORNECEDOR,'
      '  FABRICANTE = :FABRICANTE,'
      '  QTDATUAL = :QTDATUAL,'
      '  QTDMINIMA = :QTDMINIMA,'
      '  CUSTO = :CUSTO,'
      '  CUSTOMEDIO = :CUSTOMEDIO,'
      '  PRECO = :PRECO,'
      '  DT_COMPRA = :DT_COMPRA,'
      '  DT_VENDA = :DT_VENDA,'
      '  COMISSAO = :COMISSAO,'
      '  UNIDADE = :UNIDADE,'
      '  TRIBUTO = :TRIBUTO,'
      '  DESCONTO = :DESCONTO,'
      '  SERVICO = :SERVICO,'
      '  APLICACOES = :APLICACOES,'
      '  ATIVO = :ATIVO,'
      '  INSTRUCOES = :INSTRUCOES,'
      '  MARGEM_SEGURANCA = :MARGEM_SEGURANCA,'
      '  QTD_VENDA = :QTD_VENDA,'
      '  CURVA = :CURVA,'
      '  USAR_MARGEM_CURVA = :USAR_MARGEM_CURVA,'
      '  MARGEM_CURVA = :MARGEM_CURVA,'
      '  NCM = :NCM,'
      '  ETIQUETA = :ETIQUETA,'
      '  COMPRA = :COMPRA,'
      '  CST = :CST,'
      '  CSON = :CSON,'
      '  ICMS = :ICMS,'
      '  IPI = :IPI,'
      '  ST = :ST,'
      '  CURVA_ESTOQUE = :CURVA_ESTOQUE,'
      '  QTD_MAXIMA = :QTD_MAXIMA,'
      '  QTD_SEGURANCA = :QTD_SEGURANCA,'
      '  QTD_GARANTIA = :QTD_GARANTIA,'
      '  OBSERVACAO = :OBSERVACAO,'
      '  PRODUTO_FINAL = :PRODUTO_FINAL,'
      '  GERA_COMISSAO = :GERA_COMISSAO,'
      '  BAIXAR_ESTOQUE = :BAIXAR_ESTOQUE,'
      '  IPPT = :IPPT,'
      '  INGREDIENTE = :INGREDIENTE,'
      '  HASH = :HASH,'
      '  MESCLAR = :MESCLAR,'
      '  ID_SETOR = :ID_SETOR,'
      '  COR = :COR,'
      '  TAMANHO = :TAMANHO,'
      '  COLECAO = :COLECAO,'
      '  BARRAS = :BARRAS,'
      '  POSSUI_SABOR = :POSSUI_SABOR,'
      '  MAIS_PEDIDO = :MAIS_PEDIDO,'
      '  SEGMENTACAO = :SEGMENTACAO,'
      '  POSSUI_TAMANHO = :POSSUI_TAMANHO,'
      '  IPICP = :IPICP,'
      '  ICMSCP = :ICMSCP,'
      '  STCP = :STCP,'
      '  CT_VARIAVEIS = :CT_VARIAVEIS,'
      '  PRIORIDADE = :PRIORIDADE,'
      '  HORA_LIMITE = :HORA_LIMITE,'
      '  CASA_DECIMAL = :CASA_DECIMAL,'
      '  IAT = :IAT,'
      '  IMPORTADO = :IMPORTADO,'
      '  PRODUZIDO = :PRODUZIDO,'
      '  ID_COMBO = :ID_COMBO,'
      '  DOUBLE_ITEM = :DOUBLE_ITEM,'
      '  CFOP = :CFOP,'
      '  REPLICADO = :REPLICADO,'
      '  ID_TOPIN = :ID_TOPIN,'
      '  PONTO_CARNE = :PONTO_CARNE,'
      '  CODIGO_IFOOD = :CODIGO_IFOOD,'
      '  PODE_COMPRAR = :PODE_COMPRAR,'
      '  MARGEM = :MARGEM,'
      '  DOM = :DOM,'
      '  SEG = :SEG,'
      '  TER = :TER,'
      '  QUA = :QUA,'
      '  QUI = :QUI,'
      '  SEX = :SEX,'
      '  SAB = :SAB,'
      '  INICIO_VENDA = :INICIO_VENDA,'
      '  FIM_VENDA = :FIM_VENDA'
      'where'
      '  PRODUTO = :OLD_PRODUTO')
    ParamCheck = True
    UniDirectional = False
    GeneratorField.Field = 'PRODUTO'
    GeneratorField.Generator = 'ID_PRODUTO'
    GeneratorField.ApplyEvent = gamOnPost
    Left = 923
    Top = 8
    object ProdutosPRODUTO: TIBStringField
      FieldName = 'PRODUTO'
      Origin = '"EST"."PRODUTO"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 15
    end
    object ProdutosREFERENCIA: TIBStringField
      FieldName = 'REFERENCIA'
      Origin = '"EST"."REFERENCIA"'
      Size = 15
    end
    object ProdutosEQUIVALENTE: TIBStringField
      FieldName = 'EQUIVALENTE'
      Origin = '"EST"."EQUIVALENTE"'
      Size = 15
    end
    object ProdutosLOCAL: TIBStringField
      FieldName = 'LOCAL'
      Origin = '"EST"."LOCAL"'
      Size = 100
    end
    object ProdutosNOME: TIBStringField
      FieldName = 'NOME'
      Origin = '"EST"."NOME"'
      Required = True
      Size = 50
    end
    object ProdutosMARCA: TIBStringField
      FieldName = 'MARCA'
      Origin = '"EST"."MARCA"'
      Size = 15
    end
    object ProdutosAPLICACAO: TIBStringField
      FieldName = 'APLICACAO'
      Origin = '"EST"."APLICACAO"'
      Size = 50
    end
    object ProdutosGRUPO: TIntegerField
      FieldName = 'GRUPO'
      Origin = '"EST"."GRUPO"'
    end
    object ProdutosFORNECEDOR: TIntegerField
      FieldName = 'FORNECEDOR'
      Origin = '"EST"."FORNECEDOR"'
    end
    object ProdutosFABRICANTE: TIntegerField
      FieldName = 'FABRICANTE'
      Origin = '"EST"."FABRICANTE"'
    end
    object ProdutosQTDATUAL: TIBBCDField
      FieldName = 'QTDATUAL'
      Origin = '"EST"."QTDATUAL"'
      Precision = 18
      Size = 2
    end
    object ProdutosQTDMINIMA: TIBBCDField
      FieldName = 'QTDMINIMA'
      Origin = '"EST"."QTDMINIMA"'
      Precision = 18
      Size = 2
    end
    object ProdutosCUSTO: TIBBCDField
      FieldName = 'CUSTO'
      Origin = '"EST"."CUSTO"'
      Precision = 18
      Size = 4
    end
    object ProdutosCUSTOMEDIO: TIBBCDField
      FieldName = 'CUSTOMEDIO'
      Origin = '"EST"."CUSTOMEDIO"'
      Precision = 18
      Size = 4
    end
    object ProdutosPRECO: TIBBCDField
      FieldName = 'PRECO'
      Origin = '"EST"."PRECO"'
      Precision = 18
      Size = 2
    end
    object ProdutosDT_COMPRA: TDateField
      FieldName = 'DT_COMPRA'
      Origin = '"EST"."DT_COMPRA"'
    end
    object ProdutosDT_VENDA: TDateField
      FieldName = 'DT_VENDA'
      Origin = '"EST"."DT_VENDA"'
    end
    object ProdutosCOMISSAO: TIBBCDField
      FieldName = 'COMISSAO'
      Origin = '"EST"."COMISSAO"'
      Precision = 18
      Size = 2
    end
    object ProdutosUNIDADE: TIBStringField
      FieldName = 'UNIDADE'
      Origin = '"EST"."UNIDADE"'
      FixedChar = True
      Size = 2
    end
    object ProdutosTRIBUTO: TIBStringField
      FieldName = 'TRIBUTO'
      Origin = '"EST"."TRIBUTO"'
      FixedChar = True
      Size = 2
    end
    object ProdutosDESCONTO: TIBBCDField
      FieldName = 'DESCONTO'
      Origin = '"EST"."DESCONTO"'
      Precision = 18
      Size = 2
    end
    object ProdutosSERVICO: TIBStringField
      FieldName = 'SERVICO'
      Origin = '"EST"."SERVICO"'
      FixedChar = True
      Size = 1
    end
    object ProdutosAPLICACOES: TWideMemoField
      FieldName = 'APLICACOES'
      Origin = '"EST"."APLICACOES"'
      ProviderFlags = [pfInUpdate]
      BlobType = ftWideMemo
      Size = 8
    end
    object ProdutosATIVO: TIBStringField
      FieldName = 'ATIVO'
      Origin = '"EST"."ATIVO"'
      FixedChar = True
      Size = 1
    end
    object ProdutosINSTRUCOES: TWideMemoField
      FieldName = 'INSTRUCOES'
      Origin = '"EST"."INSTRUCOES"'
      ProviderFlags = [pfInUpdate]
      BlobType = ftWideMemo
      Size = 8
    end
    object ProdutosMARGEM_SEGURANCA: TIBBCDField
      FieldName = 'MARGEM_SEGURANCA'
      Origin = '"EST"."MARGEM_SEGURANCA"'
      Precision = 18
      Size = 2
    end
    object ProdutosQTD_VENDA: TIBBCDField
      FieldName = 'QTD_VENDA'
      Origin = '"EST"."QTD_VENDA"'
      Precision = 18
      Size = 2
    end
    object ProdutosCURVA: TIBStringField
      FieldName = 'CURVA'
      Origin = '"EST"."CURVA"'
      FixedChar = True
      Size = 1
    end
    object ProdutosUSAR_MARGEM_CURVA: TIBStringField
      FieldName = 'USAR_MARGEM_CURVA'
      Origin = '"EST"."USAR_MARGEM_CURVA"'
      FixedChar = True
      Size = 1
    end
    object ProdutosMARGEM_CURVA: TIntegerField
      FieldName = 'MARGEM_CURVA'
      Origin = '"EST"."MARGEM_CURVA"'
    end
    object ProdutosNCM: TIBStringField
      FieldName = 'NCM'
      Origin = '"EST"."NCM"'
      Size = 10
    end
    object ProdutosETIQUETA: TIBStringField
      FieldName = 'ETIQUETA'
      Origin = '"EST"."ETIQUETA"'
      FixedChar = True
      Size = 1
    end
    object ProdutosCOMPRA: TIBStringField
      FieldName = 'COMPRA'
      Origin = '"EST"."COMPRA"'
      FixedChar = True
      Size = 1
    end
    object ProdutosCST: TIBStringField
      FieldName = 'CST'
      Origin = '"EST"."CST"'
      Size = 5
    end
    object ProdutosCSON: TIBStringField
      FieldName = 'CSON'
      Origin = '"EST"."CSON"'
      Size = 5
    end
    object ProdutosICMS: TIntegerField
      FieldName = 'ICMS'
      Origin = '"EST"."ICMS"'
    end
    object ProdutosIPI: TIntegerField
      FieldName = 'IPI'
      Origin = '"EST"."IPI"'
    end
    object ProdutosST: TIntegerField
      FieldName = 'ST'
      Origin = '"EST"."ST"'
    end
    object ProdutosCURVA_ESTOQUE: TIBStringField
      FieldName = 'CURVA_ESTOQUE'
      Origin = '"EST"."CURVA_ESTOQUE"'
      FixedChar = True
      Size = 1
    end
    object ProdutosQTD_MAXIMA: TIntegerField
      FieldName = 'QTD_MAXIMA'
      Origin = '"EST"."QTD_MAXIMA"'
    end
    object ProdutosQTD_SEGURANCA: TIBBCDField
      FieldName = 'QTD_SEGURANCA'
      Origin = '"EST"."QTD_SEGURANCA"'
      Precision = 18
      Size = 2
    end
    object ProdutosQTD_GARANTIA: TIntegerField
      FieldName = 'QTD_GARANTIA'
      Origin = '"EST"."QTD_GARANTIA"'
    end
    object ProdutosOBSERVACAO: TWideMemoField
      FieldName = 'OBSERVACAO'
      Origin = '"EST"."OBSERVACAO"'
      ProviderFlags = [pfInUpdate]
      BlobType = ftWideMemo
      Size = 8
    end
    object ProdutosPRODUTO_FINAL: TIBStringField
      FieldName = 'PRODUTO_FINAL'
      Origin = '"EST"."PRODUTO_FINAL"'
      FixedChar = True
      Size = 1
    end
    object ProdutosGERA_COMISSAO: TIBStringField
      FieldName = 'GERA_COMISSAO'
      Origin = '"EST"."GERA_COMISSAO"'
      FixedChar = True
      Size = 1
    end
    object ProdutosBAIXAR_ESTOQUE: TIBStringField
      FieldName = 'BAIXAR_ESTOQUE'
      Origin = '"EST"."BAIXAR_ESTOQUE"'
      FixedChar = True
      Size = 1
    end
    object ProdutosIPPT: TIBStringField
      FieldName = 'IPPT'
      Origin = '"EST"."IPPT"'
      FixedChar = True
      Size = 1
    end
    object ProdutosINGREDIENTE: TIBStringField
      FieldName = 'INGREDIENTE'
      Origin = '"EST"."INGREDIENTE"'
      FixedChar = True
      Size = 1
    end
    object ProdutosHASH: TBlobField
      FieldName = 'HASH'
      Origin = '"EST"."HASH"'
      ProviderFlags = [pfInUpdate]
      Size = 8
    end
    object ProdutosMESCLAR: TIBStringField
      FieldName = 'MESCLAR'
      Origin = '"EST"."MESCLAR"'
      FixedChar = True
      Size = 1
    end
    object ProdutosID_SETOR: TIntegerField
      FieldName = 'ID_SETOR'
      Origin = '"EST"."ID_SETOR"'
      Required = True
    end
    object ProdutosCOR: TIntegerField
      FieldName = 'COR'
      Origin = '"EST"."COR"'
    end
    object ProdutosTAMANHO: TIntegerField
      FieldName = 'TAMANHO'
      Origin = '"EST"."TAMANHO"'
    end
    object ProdutosCOLECAO: TIntegerField
      FieldName = 'COLECAO'
      Origin = '"EST"."COLECAO"'
    end
    object ProdutosBARRAS: TIBStringField
      FieldName = 'BARRAS'
      Origin = '"EST"."BARRAS"'
      Size = 13
    end
    object ProdutosPOSSUI_SABOR: TIBStringField
      FieldName = 'POSSUI_SABOR'
      Origin = '"EST"."POSSUI_SABOR"'
      FixedChar = True
      Size = 1
    end
    object ProdutosMAIS_PEDIDO: TIBStringField
      FieldName = 'MAIS_PEDIDO'
      Origin = '"EST"."MAIS_PEDIDO"'
      FixedChar = True
      Size = 1
    end
    object ProdutosSEGMENTACAO: TIBStringField
      FieldName = 'SEGMENTACAO'
      Origin = '"EST"."SEGMENTACAO"'
      FixedChar = True
      Size = 1
    end
    object ProdutosPOSSUI_TAMANHO: TIBStringField
      FieldName = 'POSSUI_TAMANHO'
      Origin = '"EST"."POSSUI_TAMANHO"'
      FixedChar = True
      Size = 1
    end
    object ProdutosIPICP: TIntegerField
      FieldName = 'IPICP'
      Origin = '"EST"."IPICP"'
    end
    object ProdutosICMSCP: TIntegerField
      FieldName = 'ICMSCP'
      Origin = '"EST"."ICMSCP"'
    end
    object ProdutosSTCP: TIntegerField
      FieldName = 'STCP'
      Origin = '"EST"."STCP"'
    end
    object ProdutosCT_VARIAVEIS: TIntegerField
      FieldName = 'CT_VARIAVEIS'
      Origin = '"EST"."CT_VARIAVEIS"'
    end
    object ProdutosLUCROLIQUIDO: TIBBCDField
      FieldKind = fkInternalCalc
      FieldName = 'LUCROLIQUIDO'
      Origin = '"EST"."LUCROLIQUIDO"'
      ProviderFlags = []
      ReadOnly = True
      Precision = 18
      Size = 4
    end
    object ProdutosPRIORIDADE: TIntegerField
      FieldName = 'PRIORIDADE'
      Origin = '"EST"."PRIORIDADE"'
    end
    object ProdutosHORA_LIMITE: TTimeField
      FieldName = 'HORA_LIMITE'
      Origin = '"EST"."HORA_LIMITE"'
    end
    object ProdutosCASA_DECIMAL: TIntegerField
      FieldName = 'CASA_DECIMAL'
      Origin = '"EST"."CASA_DECIMAL"'
    end
    object ProdutosIAT: TIBStringField
      FieldName = 'IAT'
      Origin = '"EST"."IAT"'
      FixedChar = True
      Size = 1
    end
    object ProdutosIMPORTADO: TIntegerField
      FieldName = 'IMPORTADO'
      Origin = '"EST"."IMPORTADO"'
    end
    object ProdutosPRODUZIDO: TIBStringField
      FieldName = 'PRODUZIDO'
      Origin = '"EST"."PRODUZIDO"'
      FixedChar = True
      Size = 1
    end
    object ProdutosID_COMBO: TIntegerField
      FieldName = 'ID_COMBO'
      Origin = '"EST"."ID_COMBO"'
    end
    object ProdutosDOUBLE_ITEM: TIBStringField
      FieldName = 'DOUBLE_ITEM'
      Origin = '"EST"."DOUBLE_ITEM"'
      FixedChar = True
      Size = 1
    end
    object ProdutosCFOP: TIBStringField
      FieldName = 'CFOP'
      Origin = '"EST"."CFOP"'
      Size = 10
    end
    object ProdutosREPLICADO: TIBStringField
      FieldName = 'REPLICADO'
      Origin = '"EST"."REPLICADO"'
      FixedChar = True
      Size = 1
    end
    object ProdutosID_TOPIN: TIntegerField
      FieldName = 'ID_TOPIN'
      Origin = '"EST"."ID_TOPIN"'
    end
    object ProdutosPONTO_CARNE: TIBStringField
      FieldName = 'PONTO_CARNE'
      Origin = '"EST"."PONTO_CARNE"'
      FixedChar = True
      Size = 1
    end
    object ProdutosCODIGO_IFOOD: TIBStringField
      FieldName = 'CODIGO_IFOOD'
      Origin = '"EST"."CODIGO_IFOOD"'
    end
    object ProdutosPODE_COMPRAR: TIBStringField
      FieldName = 'PODE_COMPRAR'
      Origin = '"EST"."PODE_COMPRAR"'
      FixedChar = True
      Size = 1
    end
    object ProdutosMARGEM: TFloatField
      FieldName = 'MARGEM'
      Origin = '"EST"."MARGEM"'
    end
    object ProdutosDOM: TIBStringField
      FieldName = 'DOM'
      Origin = '"EST"."DOM"'
      FixedChar = True
      Size = 1
    end
    object ProdutosSEG: TIBStringField
      FieldName = 'SEG'
      Origin = '"EST"."SEG"'
      FixedChar = True
      Size = 1
    end
    object ProdutosTER: TIBStringField
      FieldName = 'TER'
      Origin = '"EST"."TER"'
      FixedChar = True
      Size = 1
    end
    object ProdutosQUA: TIBStringField
      FieldName = 'QUA'
      Origin = '"EST"."QUA"'
      FixedChar = True
      Size = 1
    end
    object ProdutosQUI: TIBStringField
      FieldName = 'QUI'
      Origin = '"EST"."QUI"'
      FixedChar = True
      Size = 1
    end
    object ProdutosSEX: TIBStringField
      FieldName = 'SEX'
      Origin = '"EST"."SEX"'
      FixedChar = True
      Size = 1
    end
    object ProdutosSAB: TIBStringField
      FieldName = 'SAB'
      Origin = '"EST"."SAB"'
      FixedChar = True
      Size = 1
    end
    object ProdutosINICIO_VENDA: TTimeField
      FieldName = 'INICIO_VENDA'
      Origin = '"EST"."INICIO_VENDA"'
    end
    object ProdutosFIM_VENDA: TTimeField
      FieldName = 'FIM_VENDA'
      Origin = '"EST"."FIM_VENDA"'
    end
  end
  object BuscaCodGrupo: TIBQuery
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select GRUPO from GRP'
      'where NOME=:NOME')
    Left = 851
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'NOME'
        ParamType = ptUnknown
      end>
    object BuscaCodGrupoGRUPO: TIntegerField
      FieldName = 'GRUPO'
      Origin = '"GRP"."GRUPO"'
    end
  end
  object GRUPOS: TIBDataSet
    BufferChunks = 1000
    CachedUpdates = False
    DeleteSQL.Strings = (
      'delete from GRP'
      'where'
      '  GRUPO = :OLD_GRUPO')
    InsertSQL.Strings = (
      'insert into GRP'
      
        '  (GRUPO, NOME, ESTOQUE, ID_IMPRESSORA, ILHA, ID_ESTOQUES, ID_FA' +
        'MILIA)'
      'values'
      
        '  (:GRUPO, :NOME, :ESTOQUE, :ID_IMPRESSORA, :ILHA, :ID_ESTOQUES,' +
        ' :ID_FAMILIA)')
    RefreshSQL.Strings = (
      'Select '
      '  GRUPO,'
      '  NOME,'
      '  ESTOQUE,'
      '  ID_IMPRESSORA,'
      '  ILHA,'
      '  ID_ESTOQUES,'
      '  ID_FAMILIA'
      'from GRP '
      'where'
      '  GRUPO = :GRUPO')
    SelectSQL.Strings = (
      'select * from GRP')
    ModifySQL.Strings = (
      'update GRP'
      'set'
      '  GRUPO = :GRUPO,'
      '  NOME = :NOME,'
      '  ESTOQUE = :ESTOQUE,'
      '  ID_IMPRESSORA = :ID_IMPRESSORA,'
      '  ILHA = :ILHA,'
      '  ID_ESTOQUES = :ID_ESTOQUES,'
      '  ID_FAMILIA = :ID_FAMILIA'
      'where'
      '  GRUPO = :OLD_GRUPO')
    ParamCheck = True
    UniDirectional = False
    GeneratorField.Field = 'GRUPO'
    GeneratorField.Generator = 'COD_GRUPO_PDT'
    GeneratorField.ApplyEvent = gamOnPost
    Left = 923
    Top = 56
    object GRUPOSGRUPO: TIntegerField
      FieldName = 'GRUPO'
      Origin = '"GRP"."GRUPO"'
    end
    object GRUPOSNOME: TIBStringField
      FieldName = 'NOME'
      Origin = '"GRP"."NOME"'
      Required = True
      Size = 25
    end
    object GRUPOSESTOQUE: TIBStringField
      FieldName = 'ESTOQUE'
      Origin = '"GRP"."ESTOQUE"'
      FixedChar = True
      Size = 1
    end
    object GRUPOSID_IMPRESSORA: TIntegerField
      FieldName = 'ID_IMPRESSORA'
      Origin = '"GRP"."ID_IMPRESSORA"'
    end
    object GRUPOSILHA: TIntegerField
      FieldName = 'ILHA'
      Origin = '"GRP"."ILHA"'
    end
    object GRUPOSID_ESTOQUES: TIntegerField
      FieldName = 'ID_ESTOQUES'
      Origin = '"GRP"."ID_ESTOQUES"'
    end
    object GRUPOSID_FAMILIA: TIntegerField
      FieldName = 'ID_FAMILIA'
      Origin = '"GRP"."ID_FAMILIA"'
    end
  end
  object CLI: TIBDataSet
    BufferChunks = 1000
    CachedUpdates = False
    DeleteSQL.Strings = (
      'delete from CLI'
      'where'
      '  CLIENTE = :OLD_CLIENTE')
    InsertSQL.Strings = (
      'insert into CLI'
      
        '  (CLIENTE, NOME, ENDERECO, BAIRRO, CIDADE, UF, CEP, CIC, DT_CAD' +
        'ASTRO, '
      
        '   DT_NASCIMENTO, DT_ULT_COMPRA, INSC_IDENT, DDD, TELEFONE, FAX,' +
        ' EMAIL, '
      
        '   BLOQUEADO, MOTIVO, P1_DE, P1_ATE, P1_VENCIMENTO, P2_DE, P2_AT' +
        'E, P2_VENCIMENTO, '
      
        '   USARLIMITE, LIMITE, DESCONTO, OBS, VALOR_DESCONTO, ECF, BOLET' +
        'O, CARTEIRA, '
      
        '   GRUPO, ROTA, TAXA_ENTREGA, CLASSIFICACAO, FRETE_POR_CONTA, FR' +
        'ETE_TIPO, '
      
        '   ACRESCIMO_NOTA, VENDEDOR, OS, TIPO_FAT, MESSAGEM_FINANCEIRO, ' +
        'ENDERECO_NUM, '
      
        '   ENDERECO_CPL, ENDERECO_COD_MUN, ENDERECO_COD_UF, TABELA, ENDE' +
        'RECO_REF, '
      
        '   ENDERECO_REF_COB, BAI_COB, CID_COB, UF_COB, ENDERECO_COD_UF_C' +
        'OB, ENDERECO_COD_MUN_COB, '
      
        '   END_COB, CEP_COB, USUARIO, SEXO, PALAVRA, MENSALIDADE, CFOP, ' +
        'CONTATO, '
      
        '   CPF_DONO, ELISEU, DATA_ELISEU, EMAIL2, EMAIL3, RAZAO_SOCIAL, ' +
        'SERVENTIA, '
      
        '   ID_INSC, CARTORIO, PAGAMENTO_PREFERENCIAL, PERIODICIDADE_PREF' +
        'ERENCIAL, '
      
        '   ENVIA_CARNE_EMAIL, ENDERECO_CPL_COB, ENDERECO_NUM_COB, TERMO_' +
        'ADESAO)'
      'values'
      
        '  (:CLIENTE, :NOME, :ENDERECO, :BAIRRO, :CIDADE, :UF, :CEP, :CIC' +
        ', :DT_CADASTRO, '
      
        '   :DT_NASCIMENTO, :DT_ULT_COMPRA, :INSC_IDENT, :DDD, :TELEFONE,' +
        ' :FAX, '
      
        '   :EMAIL, :BLOQUEADO, :MOTIVO, :P1_DE, :P1_ATE, :P1_VENCIMENTO,' +
        ' :P2_DE, '
      
        '   :P2_ATE, :P2_VENCIMENTO, :USARLIMITE, :LIMITE, :DESCONTO, :OB' +
        'S, :VALOR_DESCONTO, '
      
        '   :ECF, :BOLETO, :CARTEIRA, :GRUPO, :ROTA, :TAXA_ENTREGA, :CLAS' +
        'SIFICACAO, '
      
        '   :FRETE_POR_CONTA, :FRETE_TIPO, :ACRESCIMO_NOTA, :VENDEDOR, :O' +
        'S, :TIPO_FAT, '
      
        '   :MESSAGEM_FINANCEIRO, :ENDERECO_NUM, :ENDERECO_CPL, :ENDERECO' +
        '_COD_MUN, '
      
        '   :ENDERECO_COD_UF, :TABELA, :ENDERECO_REF, :ENDERECO_REF_COB, ' +
        ':BAI_COB, '
      
        '   :CID_COB, :UF_COB, :ENDERECO_COD_UF_COB, :ENDERECO_COD_MUN_CO' +
        'B, :END_COB, '
      
        '   :CEP_COB, :USUARIO, :SEXO, :PALAVRA, :MENSALIDADE, :CFOP, :CO' +
        'NTATO, '
      
        '   :CPF_DONO, :ELISEU, :DATA_ELISEU, :EMAIL2, :EMAIL3, :RAZAO_SO' +
        'CIAL, :SERVENTIA, '
      
        '   :ID_INSC, :CARTORIO, :PAGAMENTO_PREFERENCIAL, :PERIODICIDADE_' +
        'PREFERENCIAL, '
      
        '   :ENVIA_CARNE_EMAIL, :ENDERECO_CPL_COB, :ENDERECO_NUM_COB, :TE' +
        'RMO_ADESAO)')
    RefreshSQL.Strings = (
      'Select '
      '  CLIENTE,'
      '  NOME,'
      '  ENDERECO,'
      '  BAIRRO,'
      '  CIDADE,'
      '  UF,'
      '  CEP,'
      '  CIC,'
      '  DT_CADASTRO,'
      '  DT_NASCIMENTO,'
      '  DT_ULT_COMPRA,'
      '  INSC_IDENT,'
      '  DDD,'
      '  TELEFONE,'
      '  FAX,'
      '  EMAIL,'
      '  BLOQUEADO,'
      '  MOTIVO,'
      '  P1_DE,'
      '  P1_ATE,'
      '  P1_VENCIMENTO,'
      '  P2_DE,'
      '  P2_ATE,'
      '  P2_VENCIMENTO,'
      '  USARLIMITE,'
      '  LIMITE,'
      '  DESCONTO,'
      '  OBS,'
      '  VALOR_DESCONTO,'
      '  ECF,'
      '  BOLETO,'
      '  CARTEIRA,'
      '  GRUPO,'
      '  ROTA,'
      '  TAXA_ENTREGA,'
      '  CLASSIFICACAO,'
      '  FRETE_POR_CONTA,'
      '  FRETE_TIPO,'
      '  ACRESCIMO_NOTA,'
      '  VENDEDOR,'
      '  OS,'
      '  TIPO_FAT,'
      '  MESSAGEM_FINANCEIRO,'
      '  ENDERECO_NUM,'
      '  ENDERECO_CPL,'
      '  ENDERECO_COD_MUN,'
      '  ENDERECO_COD_UF,'
      '  TABELA,'
      '  ENDERECO_REF,'
      '  ENDERECO_REF_COB,'
      '  BAI_COB,'
      '  CID_COB,'
      '  UF_COB,'
      '  ENDERECO_COD_UF_COB,'
      '  ENDERECO_COD_MUN_COB,'
      '  END_COB,'
      '  CEP_COB,'
      '  USUARIO,'
      '  SEXO,'
      '  PALAVRA,'
      '  MENSALIDADE,'
      '  CFOP,'
      '  CONTATO,'
      '  CPF_DONO,'
      '  ELISEU,'
      '  DATA_ELISEU,'
      '  EMAIL2,'
      '  EMAIL3,'
      '  RAZAO_SOCIAL,'
      '  SERVENTIA,'
      '  ID_INSC,'
      '  CARTORIO,'
      '  PAGAMENTO_PREFERENCIAL,'
      '  PERIODICIDADE_PREFERENCIAL,'
      '  ENVIA_CARNE_EMAIL,'
      '  ENDERECO_CPL_COB,'
      '  ENDERECO_NUM_COB,'
      '  TERMO_ADESAO'
      'from CLI '
      'where'
      '  CLIENTE = :CLIENTE')
    SelectSQL.Strings = (
      'select * from CLI')
    ModifySQL.Strings = (
      'update CLI'
      'set'
      '  CLIENTE = :CLIENTE,'
      '  NOME = :NOME,'
      '  ENDERECO = :ENDERECO,'
      '  BAIRRO = :BAIRRO,'
      '  CIDADE = :CIDADE,'
      '  UF = :UF,'
      '  CEP = :CEP,'
      '  CIC = :CIC,'
      '  DT_CADASTRO = :DT_CADASTRO,'
      '  DT_NASCIMENTO = :DT_NASCIMENTO,'
      '  DT_ULT_COMPRA = :DT_ULT_COMPRA,'
      '  INSC_IDENT = :INSC_IDENT,'
      '  DDD = :DDD,'
      '  TELEFONE = :TELEFONE,'
      '  FAX = :FAX,'
      '  EMAIL = :EMAIL,'
      '  BLOQUEADO = :BLOQUEADO,'
      '  MOTIVO = :MOTIVO,'
      '  P1_DE = :P1_DE,'
      '  P1_ATE = :P1_ATE,'
      '  P1_VENCIMENTO = :P1_VENCIMENTO,'
      '  P2_DE = :P2_DE,'
      '  P2_ATE = :P2_ATE,'
      '  P2_VENCIMENTO = :P2_VENCIMENTO,'
      '  USARLIMITE = :USARLIMITE,'
      '  LIMITE = :LIMITE,'
      '  DESCONTO = :DESCONTO,'
      '  OBS = :OBS,'
      '  VALOR_DESCONTO = :VALOR_DESCONTO,'
      '  ECF = :ECF,'
      '  BOLETO = :BOLETO,'
      '  CARTEIRA = :CARTEIRA,'
      '  GRUPO = :GRUPO,'
      '  ROTA = :ROTA,'
      '  TAXA_ENTREGA = :TAXA_ENTREGA,'
      '  CLASSIFICACAO = :CLASSIFICACAO,'
      '  FRETE_POR_CONTA = :FRETE_POR_CONTA,'
      '  FRETE_TIPO = :FRETE_TIPO,'
      '  ACRESCIMO_NOTA = :ACRESCIMO_NOTA,'
      '  VENDEDOR = :VENDEDOR,'
      '  OS = :OS,'
      '  TIPO_FAT = :TIPO_FAT,'
      '  MESSAGEM_FINANCEIRO = :MESSAGEM_FINANCEIRO,'
      '  ENDERECO_NUM = :ENDERECO_NUM,'
      '  ENDERECO_CPL = :ENDERECO_CPL,'
      '  ENDERECO_COD_MUN = :ENDERECO_COD_MUN,'
      '  ENDERECO_COD_UF = :ENDERECO_COD_UF,'
      '  TABELA = :TABELA,'
      '  ENDERECO_REF = :ENDERECO_REF,'
      '  ENDERECO_REF_COB = :ENDERECO_REF_COB,'
      '  BAI_COB = :BAI_COB,'
      '  CID_COB = :CID_COB,'
      '  UF_COB = :UF_COB,'
      '  ENDERECO_COD_UF_COB = :ENDERECO_COD_UF_COB,'
      '  ENDERECO_COD_MUN_COB = :ENDERECO_COD_MUN_COB,'
      '  END_COB = :END_COB,'
      '  CEP_COB = :CEP_COB,'
      '  USUARIO = :USUARIO,'
      '  SEXO = :SEXO,'
      '  PALAVRA = :PALAVRA,'
      '  MENSALIDADE = :MENSALIDADE,'
      '  CFOP = :CFOP,'
      '  CONTATO = :CONTATO,'
      '  CPF_DONO = :CPF_DONO,'
      '  ELISEU = :ELISEU,'
      '  DATA_ELISEU = :DATA_ELISEU,'
      '  EMAIL2 = :EMAIL2,'
      '  EMAIL3 = :EMAIL3,'
      '  RAZAO_SOCIAL = :RAZAO_SOCIAL,'
      '  SERVENTIA = :SERVENTIA,'
      '  ID_INSC = :ID_INSC,'
      '  CARTORIO = :CARTORIO,'
      '  PAGAMENTO_PREFERENCIAL = :PAGAMENTO_PREFERENCIAL,'
      '  PERIODICIDADE_PREFERENCIAL = :PERIODICIDADE_PREFERENCIAL,'
      '  ENVIA_CARNE_EMAIL = :ENVIA_CARNE_EMAIL,'
      '  ENDERECO_CPL_COB = :ENDERECO_CPL_COB,'
      '  ENDERECO_NUM_COB = :ENDERECO_NUM_COB,'
      '  TERMO_ADESAO = :TERMO_ADESAO'
      'where'
      '  CLIENTE = :OLD_CLIENTE')
    ParamCheck = True
    UniDirectional = False
    GeneratorField.Field = 'CLIENTE'
    GeneratorField.Generator = 'COD_CLIENTE'
    GeneratorField.ApplyEvent = gamOnPost
    Left = 827
    Top = 40
    object CLICLIENTE: TIntegerField
      FieldName = 'CLIENTE'
      Origin = '"CLI"."CLIENTE"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object CLINOME: TIBStringField
      FieldName = 'NOME'
      Origin = '"CLI"."NOME"'
      Required = True
      Size = 40
    end
    object CLIENDERECO: TIBStringField
      FieldName = 'ENDERECO'
      Origin = '"CLI"."ENDERECO"'
      Size = 100
    end
    object CLIBAIRRO: TIBStringField
      FieldName = 'BAIRRO'
      Origin = '"CLI"."BAIRRO"'
    end
    object CLICIDADE: TIBStringField
      FieldName = 'CIDADE'
      Origin = '"CLI"."CIDADE"'
      Size = 60
    end
    object CLIUF: TIBStringField
      FieldName = 'UF'
      Origin = '"CLI"."UF"'
      FixedChar = True
      Size = 2
    end
    object CLICEP: TIBStringField
      FieldName = 'CEP'
      Origin = '"CLI"."CEP"'
      Size = 9
    end
    object CLICIC: TIBStringField
      FieldName = 'CIC'
      Origin = '"CLI"."CIC"'
    end
    object CLIDT_CADASTRO: TDateField
      FieldName = 'DT_CADASTRO'
      Origin = '"CLI"."DT_CADASTRO"'
    end
    object CLIDT_NASCIMENTO: TDateField
      FieldName = 'DT_NASCIMENTO'
      Origin = '"CLI"."DT_NASCIMENTO"'
    end
    object CLIDT_ULT_COMPRA: TDateField
      FieldName = 'DT_ULT_COMPRA'
      Origin = '"CLI"."DT_ULT_COMPRA"'
    end
    object CLIINSC_IDENT: TIBStringField
      FieldName = 'INSC_IDENT'
      Origin = '"CLI"."INSC_IDENT"'
    end
    object CLIDDD: TIBStringField
      FieldName = 'DDD'
      Origin = '"CLI"."DDD"'
      FixedChar = True
      Size = 2
    end
    object CLITELEFONE: TIBStringField
      FieldName = 'TELEFONE'
      Origin = '"CLI"."TELEFONE"'
      Size = 25
    end
    object CLIFAX: TIBStringField
      FieldName = 'FAX'
      Origin = '"CLI"."FAX"'
    end
    object CLIEMAIL: TIBStringField
      FieldName = 'EMAIL'
      Origin = '"CLI"."EMAIL"'
      Size = 100
    end
    object CLIBLOQUEADO: TIBStringField
      FieldName = 'BLOQUEADO'
      Origin = '"CLI"."BLOQUEADO"'
      FixedChar = True
      Size = 1
    end
    object CLIMOTIVO: TIBStringField
      FieldName = 'MOTIVO'
      Origin = '"CLI"."MOTIVO"'
      Size = 255
    end
    object CLIP1_DE: TIntegerField
      FieldName = 'P1_DE'
      Origin = '"CLI"."P1_DE"'
    end
    object CLIP1_ATE: TIntegerField
      FieldName = 'P1_ATE'
      Origin = '"CLI"."P1_ATE"'
    end
    object CLIP1_VENCIMENTO: TIntegerField
      FieldName = 'P1_VENCIMENTO'
      Origin = '"CLI"."P1_VENCIMENTO"'
    end
    object CLIP2_DE: TIntegerField
      FieldName = 'P2_DE'
      Origin = '"CLI"."P2_DE"'
    end
    object CLIP2_ATE: TIntegerField
      FieldName = 'P2_ATE'
      Origin = '"CLI"."P2_ATE"'
    end
    object CLIP2_VENCIMENTO: TIntegerField
      FieldName = 'P2_VENCIMENTO'
      Origin = '"CLI"."P2_VENCIMENTO"'
    end
    object CLIUSARLIMITE: TIBStringField
      FieldName = 'USARLIMITE'
      Origin = '"CLI"."USARLIMITE"'
      FixedChar = True
      Size = 1
    end
    object CLILIMITE: TIBBCDField
      FieldName = 'LIMITE'
      Origin = '"CLI"."LIMITE"'
      Precision = 18
      Size = 2
    end
    object CLIDESCONTO: TIBStringField
      FieldName = 'DESCONTO'
      Origin = '"CLI"."DESCONTO"'
      FixedChar = True
      Size = 1
    end
    object CLIOBS: TWideMemoField
      FieldName = 'OBS'
      Origin = '"CLI"."OBS"'
      ProviderFlags = [pfInUpdate]
      BlobType = ftWideMemo
      Size = 8
    end
    object CLIVALOR_DESCONTO: TIntegerField
      FieldName = 'VALOR_DESCONTO'
      Origin = '"CLI"."VALOR_DESCONTO"'
    end
    object CLIECF: TIBStringField
      FieldName = 'ECF'
      Origin = '"CLI"."ECF"'
      FixedChar = True
      Size = 1
    end
    object CLIBOLETO: TIBStringField
      FieldName = 'BOLETO'
      Origin = '"CLI"."BOLETO"'
      FixedChar = True
      Size = 1
    end
    object CLICARTEIRA: TIBStringField
      FieldName = 'CARTEIRA'
      Origin = '"CLI"."CARTEIRA"'
      FixedChar = True
      Size = 1
    end
    object CLIGRUPO: TIntegerField
      FieldName = 'GRUPO'
      Origin = '"CLI"."GRUPO"'
    end
    object CLIROTA: TIntegerField
      FieldName = 'ROTA'
      Origin = '"CLI"."ROTA"'
    end
    object CLITAXA_ENTREGA: TFloatField
      FieldName = 'TAXA_ENTREGA'
      Origin = '"CLI"."TAXA_ENTREGA"'
    end
    object CLICLASSIFICACAO: TIntegerField
      FieldName = 'CLASSIFICACAO'
      Origin = '"CLI"."CLASSIFICACAO"'
    end
    object CLIFRETE_POR_CONTA: TIBStringField
      FieldName = 'FRETE_POR_CONTA'
      Origin = '"CLI"."FRETE_POR_CONTA"'
      FixedChar = True
      Size = 1
    end
    object CLIFRETE_TIPO: TIBStringField
      FieldName = 'FRETE_TIPO'
      Origin = '"CLI"."FRETE_TIPO"'
      FixedChar = True
      Size = 1
    end
    object CLIACRESCIMO_NOTA: TIntegerField
      FieldName = 'ACRESCIMO_NOTA'
      Origin = '"CLI"."ACRESCIMO_NOTA"'
    end
    object CLIVENDEDOR: TIntegerField
      FieldName = 'VENDEDOR'
      Origin = '"CLI"."VENDEDOR"'
    end
    object CLIOS: TIBStringField
      FieldName = 'OS'
      Origin = '"CLI"."OS"'
      FixedChar = True
      Size = 1
    end
    object CLITIPO_FAT: TIBStringField
      FieldName = 'TIPO_FAT'
      Origin = '"CLI"."TIPO_FAT"'
      Size = 10
    end
    object CLIMESSAGEM_FINANCEIRO: TIBStringField
      FieldName = 'MESSAGEM_FINANCEIRO'
      Origin = '"CLI"."MESSAGEM_FINANCEIRO"'
      Size = 80
    end
    object CLIENDERECO_NUM: TIBStringField
      FieldName = 'ENDERECO_NUM'
      Origin = '"CLI"."ENDERECO_NUM"'
      Size = 10
    end
    object CLIENDERECO_CPL: TIBStringField
      FieldName = 'ENDERECO_CPL'
      Origin = '"CLI"."ENDERECO_CPL"'
      Size = 10
    end
    object CLIENDERECO_COD_MUN: TIntegerField
      FieldName = 'ENDERECO_COD_MUN'
      Origin = '"CLI"."ENDERECO_COD_MUN"'
    end
    object CLIENDERECO_COD_UF: TIntegerField
      FieldName = 'ENDERECO_COD_UF'
      Origin = '"CLI"."ENDERECO_COD_UF"'
    end
    object CLITABELA: TIBStringField
      FieldName = 'TABELA'
      Origin = '"CLI"."TABELA"'
      Size = 100
    end
    object CLIENDERECO_REF: TWideMemoField
      FieldName = 'ENDERECO_REF'
      Origin = '"CLI"."ENDERECO_REF"'
      ProviderFlags = [pfInUpdate]
      BlobType = ftWideMemo
      Size = 8
    end
    object CLIENDERECO_REF_COB: TBlobField
      FieldName = 'ENDERECO_REF_COB'
      Origin = '"CLI"."ENDERECO_REF_COB"'
      ProviderFlags = [pfInUpdate]
      Size = 8
    end
    object CLIBAI_COB: TIBStringField
      FieldName = 'BAI_COB'
      Origin = '"CLI"."BAI_COB"'
      Size = 35
    end
    object CLICID_COB: TIBStringField
      FieldName = 'CID_COB'
      Origin = '"CLI"."CID_COB"'
      Size = 35
    end
    object CLIUF_COB: TIBStringField
      FieldName = 'UF_COB'
      Origin = '"CLI"."UF_COB"'
      FixedChar = True
      Size = 2
    end
    object CLIENDERECO_COD_UF_COB: TIntegerField
      FieldName = 'ENDERECO_COD_UF_COB'
      Origin = '"CLI"."ENDERECO_COD_UF_COB"'
    end
    object CLIENDERECO_COD_MUN_COB: TIntegerField
      FieldName = 'ENDERECO_COD_MUN_COB'
      Origin = '"CLI"."ENDERECO_COD_MUN_COB"'
    end
    object CLIEND_COB: TIBStringField
      FieldName = 'END_COB'
      Origin = '"CLI"."END_COB"'
      Size = 100
    end
    object CLICEP_COB: TIBStringField
      FieldName = 'CEP_COB'
      Origin = '"CLI"."CEP_COB"'
      Size = 9
    end
    object CLIUSUARIO: TIBStringField
      FieldName = 'USUARIO'
      Origin = '"CLI"."USUARIO"'
      Size = 50
    end
    object CLISEXO: TIBStringField
      FieldName = 'SEXO'
      Origin = '"CLI"."SEXO"'
      FixedChar = True
      Size = 1
    end
    object CLIPALAVRA: TIBStringField
      FieldName = 'PALAVRA'
      Origin = '"CLI"."PALAVRA"'
    end
    object CLIMENSALIDADE: TFloatField
      FieldName = 'MENSALIDADE'
      Origin = '"CLI"."MENSALIDADE"'
    end
    object CLICFOP: TIBStringField
      FieldName = 'CFOP'
      Origin = '"CLI"."CFOP"'
      Size = 6
    end
    object CLICONTATO: TIBStringField
      FieldName = 'CONTATO'
      Origin = '"CLI"."CONTATO"'
      Size = 50
    end
    object CLICPF_DONO: TIBStringField
      FieldName = 'CPF_DONO'
      Origin = '"CLI"."CPF_DONO"'
      Size = 30
    end
    object CLIELISEU: TIBStringField
      FieldName = 'ELISEU'
      Origin = '"CLI"."ELISEU"'
      Size = 1
    end
    object CLIDATA_ELISEU: TDateField
      FieldName = 'DATA_ELISEU'
      Origin = '"CLI"."DATA_ELISEU"'
    end
    object CLIEMAIL2: TIBStringField
      FieldName = 'EMAIL2'
      Origin = '"CLI"."EMAIL2"'
      Size = 80
    end
    object CLIEMAIL3: TIBStringField
      FieldName = 'EMAIL3'
      Origin = '"CLI"."EMAIL3"'
      Size = 80
    end
    object CLIRAZAO_SOCIAL: TIBStringField
      FieldName = 'RAZAO_SOCIAL'
      Origin = '"CLI"."RAZAO_SOCIAL"'
      Size = 100
    end
    object CLISERVENTIA: TIntegerField
      FieldName = 'SERVENTIA'
      Origin = '"CLI"."SERVENTIA"'
    end
    object CLIID_INSC: TIntegerField
      FieldName = 'ID_INSC'
      Origin = '"CLI"."ID_INSC"'
    end
    object CLICARTORIO: TIBStringField
      FieldName = 'CARTORIO'
      Origin = '"CLI"."CARTORIO"'
      Size = 100
    end
    object CLIPAGAMENTO_PREFERENCIAL: TIBStringField
      FieldName = 'PAGAMENTO_PREFERENCIAL'
      Origin = '"CLI"."PAGAMENTO_PREFERENCIAL"'
      Size = 50
    end
    object CLIPERIODICIDADE_PREFERENCIAL: TIBStringField
      FieldName = 'PERIODICIDADE_PREFERENCIAL'
      Origin = '"CLI"."PERIODICIDADE_PREFERENCIAL"'
      Size = 50
    end
    object CLIENVIA_CARNE_EMAIL: TIBStringField
      FieldName = 'ENVIA_CARNE_EMAIL'
      Origin = '"CLI"."ENVIA_CARNE_EMAIL"'
      FixedChar = True
      Size = 1
    end
    object CLIENDERECO_CPL_COB: TIBStringField
      FieldName = 'ENDERECO_CPL_COB'
      Origin = '"CLI"."ENDERECO_CPL_COB"'
      Size = 30
    end
    object CLIENDERECO_NUM_COB: TIBStringField
      FieldName = 'ENDERECO_NUM_COB'
      Origin = '"CLI"."ENDERECO_NUM_COB"'
      Size = 30
    end
    object CLITERMO_ADESAO: TIBStringField
      FieldName = 'TERMO_ADESAO'
      Origin = '"CLI"."TERMO_ADESAO"'
      FixedChar = True
      Size = 1
    end
  end
  object FNC: TIBDataSet
    BufferChunks = 1000
    CachedUpdates = False
    DeleteSQL.Strings = (
      'delete from FNC'
      'where'
      '  FORNECEDOR = :OLD_FORNECEDOR')
    InsertSQL.Strings = (
      'insert into FNC'
      
        '  (FORNECEDOR, NOME, CONTATO, ENDERECO, BAIRRO, CIDADE, UF, CEP,' +
        ' CGC, INSCRICAO, '
      
        '   TELEFONE, OBS, ENDERECO_NUM, ENDERECO_CPL, ENDERECO_COD_MUN, ' +
        'ENDERECO_COD_UF, '
      '   TIPO, TRANS_ANTT, RAZAO_SOCIAL, TAXA, EMAIL)'
      'values'
      
        '  (:FORNECEDOR, :NOME, :CONTATO, :ENDERECO, :BAIRRO, :CIDADE, :U' +
        'F, :CEP, '
      
        '   :CGC, :INSCRICAO, :TELEFONE, :OBS, :ENDERECO_NUM, :ENDERECO_C' +
        'PL, :ENDERECO_COD_MUN, '
      
        '   :ENDERECO_COD_UF, :TIPO, :TRANS_ANTT, :RAZAO_SOCIAL, :TAXA, :' +
        'EMAIL)')
    RefreshSQL.Strings = (
      'Select '
      '  FORNECEDOR,'
      '  NOME,'
      '  CONTATO,'
      '  ENDERECO,'
      '  BAIRRO,'
      '  CIDADE,'
      '  UF,'
      '  CEP,'
      '  CGC,'
      '  INSCRICAO,'
      '  TELEFONE,'
      '  OBS,'
      '  ENDERECO_NUM,'
      '  ENDERECO_CPL,'
      '  ENDERECO_COD_MUN,'
      '  ENDERECO_COD_UF,'
      '  TIPO,'
      '  TRANS_ANTT,'
      '  RAZAO_SOCIAL,'
      '  TAXA,'
      '  EMAIL'
      'from FNC '
      'where'
      '  FORNECEDOR = :FORNECEDOR')
    SelectSQL.Strings = (
      'select * from FNC')
    ModifySQL.Strings = (
      'update FNC'
      'set'
      '  FORNECEDOR = :FORNECEDOR,'
      '  NOME = :NOME,'
      '  CONTATO = :CONTATO,'
      '  ENDERECO = :ENDERECO,'
      '  BAIRRO = :BAIRRO,'
      '  CIDADE = :CIDADE,'
      '  UF = :UF,'
      '  CEP = :CEP,'
      '  CGC = :CGC,'
      '  INSCRICAO = :INSCRICAO,'
      '  TELEFONE = :TELEFONE,'
      '  OBS = :OBS,'
      '  ENDERECO_NUM = :ENDERECO_NUM,'
      '  ENDERECO_CPL = :ENDERECO_CPL,'
      '  ENDERECO_COD_MUN = :ENDERECO_COD_MUN,'
      '  ENDERECO_COD_UF = :ENDERECO_COD_UF,'
      '  TIPO = :TIPO,'
      '  TRANS_ANTT = :TRANS_ANTT,'
      '  RAZAO_SOCIAL = :RAZAO_SOCIAL,'
      '  TAXA = :TAXA,'
      '  EMAIL = :EMAIL'
      'where'
      '  FORNECEDOR = :OLD_FORNECEDOR')
    ParamCheck = True
    UniDirectional = False
    GeneratorField.Field = 'FORNECEDOR'
    GeneratorField.Generator = 'COD_FORNECEDOR'
    GeneratorField.ApplyEvent = gamOnPost
    Left = 875
    Top = 8
    object FNCFORNECEDOR: TIntegerField
      FieldName = 'FORNECEDOR'
      Origin = '"FNC"."FORNECEDOR"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FNCNOME: TIBStringField
      FieldName = 'NOME'
      Origin = '"FNC"."NOME"'
      Size = 40
    end
    object FNCCONTATO: TIBStringField
      FieldName = 'CONTATO'
      Origin = '"FNC"."CONTATO"'
      Size = 40
    end
    object FNCENDERECO: TIBStringField
      FieldName = 'ENDERECO'
      Origin = '"FNC"."ENDERECO"'
      Size = 40
    end
    object FNCBAIRRO: TIBStringField
      FieldName = 'BAIRRO'
      Origin = '"FNC"."BAIRRO"'
    end
    object FNCCIDADE: TIBStringField
      FieldName = 'CIDADE'
      Origin = '"FNC"."CIDADE"'
    end
    object FNCUF: TIBStringField
      FieldName = 'UF'
      Origin = '"FNC"."UF"'
      Size = 2
    end
    object FNCCEP: TIBStringField
      FieldName = 'CEP'
      Origin = '"FNC"."CEP"'
      Size = 9
    end
    object FNCCGC: TIBStringField
      FieldName = 'CGC'
      Origin = '"FNC"."CGC"'
      Size = 18
    end
    object FNCINSCRICAO: TIBStringField
      FieldName = 'INSCRICAO'
      Origin = '"FNC"."INSCRICAO"'
      Size = 18
    end
    object FNCTELEFONE: TIBStringField
      FieldName = 'TELEFONE'
      Origin = '"FNC"."TELEFONE"'
      Size = 100
    end
    object FNCOBS: TWideMemoField
      FieldName = 'OBS'
      Origin = '"FNC"."OBS"'
      ProviderFlags = [pfInUpdate]
      BlobType = ftWideMemo
      Size = 8
    end
    object FNCENDERECO_NUM: TIBStringField
      FieldName = 'ENDERECO_NUM'
      Origin = '"FNC"."ENDERECO_NUM"'
      Size = 10
    end
    object FNCENDERECO_CPL: TIBStringField
      FieldName = 'ENDERECO_CPL'
      Origin = '"FNC"."ENDERECO_CPL"'
      Size = 10
    end
    object FNCENDERECO_COD_MUN: TIntegerField
      FieldName = 'ENDERECO_COD_MUN'
      Origin = '"FNC"."ENDERECO_COD_MUN"'
    end
    object FNCENDERECO_COD_UF: TIntegerField
      FieldName = 'ENDERECO_COD_UF'
      Origin = '"FNC"."ENDERECO_COD_UF"'
    end
    object FNCTIPO: TIBStringField
      FieldName = 'TIPO'
      Origin = '"FNC"."TIPO"'
      Size = 1
    end
    object FNCTRANS_ANTT: TIBStringField
      FieldName = 'TRANS_ANTT'
      Origin = '"FNC"."TRANS_ANTT"'
    end
    object FNCRAZAO_SOCIAL: TIBStringField
      FieldName = 'RAZAO_SOCIAL'
      Origin = '"FNC"."RAZAO_SOCIAL"'
      Size = 80
    end
    object FNCTAXA: TFloatField
      FieldName = 'TAXA'
      Origin = '"FNC"."TAXA"'
    end
    object FNCEMAIL: TIBStringField
      FieldName = 'EMAIL'
      Origin = '"FNC"."EMAIL"'
      Size = 100
    end
    object FNCATIVO: TIBStringField
      FieldName = 'ATIVO'
      Origin = '"FNC"."ATIVO"'
      Size = 1
    end
  end
  object TAMANHO: TIBDataSet
    BufferChunks = 1000
    CachedUpdates = False
    DeleteSQL.Strings = (
      'delete from TAMANHOS'
      'where'
      '  ID_TAMANHO = :OLD_ID_TAMANHO')
    InsertSQL.Strings = (
      'insert into TAMANHOS'
      '  (ID_TAMANHO, TAMANHO, PERC_PROPORCAO)'
      'values'
      '  (:ID_TAMANHO, :TAMANHO, :PERC_PROPORCAO)')
    RefreshSQL.Strings = (
      'Select '
      '  ID_TAMANHO,'
      '  TAMANHO,'
      '  PERC_PROPORCAO'
      'from TAMANHOS '
      'where'
      '  ID_TAMANHO = :ID_TAMANHO')
    SelectSQL.Strings = (
      'select * from TAMANHOS')
    ModifySQL.Strings = (
      'update TAMANHOS'
      'set'
      '  ID_TAMANHO = :ID_TAMANHO,'
      '  TAMANHO = :TAMANHO,'
      '  PERC_PROPORCAO = :PERC_PROPORCAO'
      'where'
      '  ID_TAMANHO = :OLD_ID_TAMANHO')
    ParamCheck = True
    UniDirectional = False
    GeneratorField.Field = 'ID_TAMANHO'
    GeneratorField.Generator = 'ID_TAMANHO'
    GeneratorField.ApplyEvent = gamOnPost
    Left = 875
    Top = 64
    object TAMANHOID_TAMANHO: TIntegerField
      FieldName = 'ID_TAMANHO'
      Origin = '"TAMANHOS"."ID_TAMANHO"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object TAMANHOTAMANHO: TIBStringField
      FieldName = 'TAMANHO'
      Origin = '"TAMANHOS"."TAMANHO"'
      Size = 100
    end
    object TAMANHOPERC_PROPORCAO: TIBBCDField
      FieldName = 'PERC_PROPORCAO'
      Origin = '"TAMANHOS"."PERC_PROPORCAO"'
      Precision = 18
      Size = 2
    end
  end
  object BuscaCodTamanho: TIBQuery
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select ID_TAMANHO from TAMANHOS'
      'where TAMANHO=:TAMANHO')
    Left = 851
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'TAMANHO'
        ParamType = ptUnknown
      end>
    object BuscaCodTamanhoID_TAMANHO: TIntegerField
      FieldName = 'ID_TAMANHO'
      Origin = '"TAMANHOS"."ID_TAMANHO"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
  object cdsReceitas: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 923
    Top = 173
    object cdsReceitasDATA_VENDA: TDateField
      DisplayWidth = 10
      FieldName = 'DATA_VENDA'
    end
    object cdsReceitasHORA_VENDA: TTimeField
      DisplayWidth = 10
      FieldName = 'HORA_VENDA'
    end
    object cdsReceitasTIPO_PAGAMENTO: TStringField
      DisplayWidth = 30
      FieldName = 'TIPO_PAGAMENTO'
      Size = 30
    end
    object cdsReceitasID_OPERADORA: TStringField
      DisplayWidth = 30
      FieldName = 'ID_OPERADORA'
      Size = 30
    end
    object cdsReceitasBANDEIRA: TStringField
      DisplayWidth = 30
      FieldName = 'BANDEIRA'
      Size = 30
    end
    object cdsReceitasVALOR_BRUTO: TFloatField
      DisplayWidth = 10
      FieldName = 'VALOR_BRUTO'
    end
    object cdsReceitasVALOR_LIQUIDO: TFloatField
      DisplayWidth = 10
      FieldName = 'VALOR_LIQUIDO'
    end
    object cdsReceitasN_CARTAO: TStringField
      DisplayWidth = 30
      FieldName = 'N_CARTAO'
      Size = 30
    end
    object cdsReceitasOBS: TStringField
      DisplayWidth = 100
      FieldName = 'OBS'
      Size = 100
    end
  end
  object dsReceitas: TwwDataSource
    DataSet = cdsReceitas
    Left = 923
    Top = 125
  end
end
