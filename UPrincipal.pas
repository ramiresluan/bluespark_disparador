﻿unit UPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, FireDAC.Stan.Intf,
  Data.DB, Vcl.Grids, Vcl.DBGrids,Rest.JSON, Vcl.ExtCtrls, vcl.wwdbigrd,
  vcl.wwdbgrid, vcl.Wwdatainspector, Vcl.PlatformDefaultStyleActnCtrls,
  Vcl.Menus, Vcl.ActnPopup, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  System.ImageList, Vcl.ImgList, acAlphaImageList, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.DBCtrls, acImage, dxGDIPlusClasses,
  FireDAC.Stan.StorageBin, IBX.IBCustomDataSet, IdComponent, IdBaseComponent,
  IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase, IdFTP,
  Vcl.ComCtrls, Vcl.WinXCtrls, sSpeedButton,ActiveX,MsHtml, Vcl.OleCtrls,
  SHDocVw, Vcl.Mask, DBAccess, Uni, MemDS, acPNG;

const
  IDM_MARCADOR = 2184;
  IDM_MARCADOR_LISTA = 2185;
  IDM_OUTDENT = 2187;
  IDM_INDENT = 2186;
  IDM_ALINHARESQ = 59;
  IDM_CENTRALIZAR = 57;
  IDM_ALINHADIR = 60;
  IDM_IMAGEM = 2168;
  IDM_LINHAHORIZ = 2150;
  IDM_RECORTAR = 16;
  IDM_COPIAR = 15;
  IDM_COLAR = 26;
  IDM_HYPERLINK = 2124;
  IDM_DESFAZER = 43;

type
  TFPrincipal = class(TForm)
    pnFundo: TPanel;
    pnTopo: TPanel;
    LbTitulo: TLabel;
    pnRodape: TPanel;
    pnClient: TPanel;
    SbMinimizar: TSpeedButton;
    SbFechar: TSpeedButton;
    svMenu: TSplitView;
    Label1: TLabel;
    Panel9: TPanel;
    Panel3: TPanel;
    PnCadastro: TPanel;
    Panel1: TPanel;
    LbConexao: TLabel;
    PcMenuPrincipal: TPageControl;
    TbEnvio: TTabSheet;
    TbCadCli: TTabSheet;
    TbCadUsu: TTabSheet;
    TbRelatorio: TTabSheet;
    TbPrincipal: TTabSheet;
    PnCadUSU: TPanel;
    PnRel: TPanel;
    PnSubTitulo: TPanel;
    PnlIncluirUsuario: TPanel;
    PnlAlterarUsuario: TPanel;
    PnlExcluirUsuario: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel7: TPanel;
    GridClientes: TDBGrid;
    DBUsuario: TDBGrid;
    Panel8: TPanel;
    PnItalico: TPanel;
    PnNegrito: TPanel;
    PnSublinhado: TPanel;
    PnCor: TPanel;
    PnCresc: TPanel;
    PnDecres: TPanel;
    PnMarcador: TPanel;
    PnNunList: TPanel;
    PnDireita: TPanel;
    PnCentro: TPanel;
    PnEsquer: TPanel;
    PnLinhaHorizontal: TPanel;
    PnFoto: TPanel;
    Lbfonte: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    CbFonte: TComboBox;
    LbTamanho: TLabel;
    CbTamanho: TComboBox;
    PnLImpar: TPanel;
    PnHiperLink: TPanel;
    WebBrowser1: TWebBrowser;
    ColorDialog: TColorDialog;
    OpenDialog: TOpenDialog;
    Panel10: TPanel;
    LbDe: TLabel;
    Label2: TLabel;
    LbAssunto: TLabel;
    LbLista: TLabel;
    LbAgendamento: TLabel;
    DtAgendamento: TDateTimePicker;
    DtAgendamentohora: TDateTimePicker;
    UdsUsuario: TUniDataSource;
    UdsCLientes: TUniDataSource;
    Panel6: TPanel;
    pEnviar: TPanel;
    plimpar: TPanel;
    UdsEnvios: TUniDataSource;
    UdnCategoria: TUniDataSource;
    edNome: TEdit;
    Label18: TLabel;
    EdAssunto: TEdit;
    EdEmail: TEdit;
    wwDBGrid1: TwwDBGrid;
    dsUniCategorias: TDataSource;
    Image1: TImage;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    pEditarlista: TPanel;
    EdGroupList: TEdit;
    LbMenu: TLabel;
    Image3: TImage;
    Label9: TLabel;
    PnAnexo: TPanel;
    Label10: TLabel;
    SvAnexo: TSplitView;
    PAnexo3: TPanel;
    Panexo2: TPanel;
    PAnexo1: TPanel;
    AddAnexo: TPanel;
    AddAnexo2: TPanel;
    AddAnexo3: TPanel;
    PAnexo7: TPanel;
    PAnexo6: TPanel;
    AddAnexo6: TPanel;
    AddAnexo7: TPanel;
    PAnexo5: TPanel;
    AddAnexo5: TPanel;
    PAnexo4: TPanel;
    AddAnexo4: TPanel;
    //procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SbMinimizarClick(Sender: TObject);
    //procedure SbAtualizaClick(Sender: TObject);
    procedure SbFecharClick(Sender: TObject);
    procedure LbMenuClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Panel1Click(Sender: TObject);
    procedure PnCadastroClick(Sender: TObject);
    procedure PnCadUSUClick(Sender: TObject);
    procedure PnRelClick(Sender: TObject);
    procedure DocumentoEmBranco(WebBrowser: TWebBrowser);
    function GetIEHandle(WebBrowser: TWebbrowser; ClassName: string): HWND;
    function  UltimoCliente: Integer;
    procedure FormCreate(Sender: TObject);
    procedure PnNegritoClick(Sender: TObject);
    procedure PnItalicoClick(Sender: TObject);
    procedure PnSublinhadoClick(Sender: TObject);
    procedure PnCorClick(Sender: TObject);
    procedure PnNunListClick(Sender: TObject);
    procedure PnMarcadorClick(Sender: TObject);
    procedure PnDecresClick(Sender: TObject);
    procedure PnCrescClick(Sender: TObject);
    procedure PnEsquerClick(Sender: TObject);
    procedure PnCentroClick(Sender: TObject);
    procedure PnDireitaClick(Sender: TObject);
    procedure PnFotoClick(Sender: TObject);
    procedure PnLinhaHorizontalClick(Sender: TObject);
    procedure PnLImparClick(Sender: TObject);
    procedure PnHiperLinkClick(Sender: TObject);
    procedure CbFonteChange(Sender: TObject);
    procedure CbTamanhoChange(Sender: TObject);
    procedure Panel5Click(Sender: TObject);
    procedure Panel6Click(Sender: TObject);
    procedure PnlIncluirUsuarioClick(Sender: TObject);
    procedure PnlAlterarUsuarioClick(Sender: TObject);
    procedure pEnviarClick(Sender: TObject);
    procedure WBLoadHTML(WebBrowser: TWebBrowser; HTMLCode: string);
    procedure edNomeEnter(Sender: TObject);
    procedure edNomeExit(Sender: TObject);
    procedure edNomeKeyPress(Sender: TObject; var Key: Char);
    procedure Panel7Click(Sender: TObject);
    procedure PnlExcluirUsuarioClick(Sender: TObject);
    procedure Label18Click(Sender: TObject);
    procedure AtualizarTelaCli;
    procedure AtualizarTelaCategoria;
    procedure VariosEmails;
    procedure wwDBGrid1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure wwDBGrid1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1Click(Sender: TObject);
    procedure LbTituloDblClick(Sender: TObject);
    procedure TbPrincipalContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure GridClientesTitleClick(Column: TColumn);
    procedure DBUsuarioTitleClick(Column: TColumn);
    procedure plimparClick(Sender: TObject);
    procedure pEditarlistaClick(Sender: TObject);
    procedure EdGroupListEnter(Sender: TObject);
    procedure EdGroupListExit(Sender: TObject);
    procedure EdGroupListKeyPress(Sender: TObject; var Key: Char);
    procedure PnAnexoClick(Sender: TObject);
    procedure AddAnexoClick(Sender: TObject);
    procedure AddAnexo2Click(Sender: TObject);
    procedure AddAnexo3Click(Sender: TObject);
    procedure AddAnexo4Click(Sender: TObject);
    procedure AddAnexo5Click(Sender: TObject);
    procedure AddAnexo6Click(Sender: TObject);
    procedure AddAnexo7Click(Sender: TObject);


  private
    { Private declarations }
  public
    { Public declarations }
    Id_Mensagem: Integer;
    Sistema_Aberto : Boolean;
  end;

var
  FPrincipal: TFPrincipal;
  vHTMLDocumento:IHTMLDocument2;
  vEmails,vCategorias :AnsiString;
  vANEXO: string;

implementation

{$R *.dfm}

uses Udm, UGeralPAF, Winapi.ShellAPI,UCadastroClientes,UcadastroUsuario,Disparador_email,Uimportador,
  UAnexos;


procedure TFPrincipal.AtualizarTelaCategoria;
begin
//
end;

procedure TFPrincipal.AtualizarTelaCli;
begin
    dm.CLIENTES.Close;
    dm.CLIENTES.SQL.Clear;
    dm.CLIENTES.SQL.Add('SELECT * FROM ClIENTE');
    dm.CLIENTES.SQL.Add('ORDER BY CAST(CLIENTEID AS INTEGER)');
    dm.CLIENTES.Open;
end;

procedure TFPrincipal.CbFonteChange(Sender: TObject);
begin
    vHTMLDocumento := WebBrowser1.Document as IHTMLDocument2;
    vHTMLDocumento.execCommand('FontName', False,CbFonte.Text);
end;

procedure TFPrincipal.CbTamanhoChange(Sender: TObject);
begin
    vHTMLDocumento := WebBrowser1.Document as IHTMLDocument2;
    case CbTamanho.ItemIndex of
      0: vHTMLDocumento.execCommand('FontSize', False,1);
      1: vHTMLDocumento.execCommand('FontSize', False,2);
      2: vHTMLDocumento.execCommand('FontSize', False,3);
      3: vHTMLDocumento.execCommand('FontSize', False,5);
      4: vHTMLDocumento.execCommand('FontSize', False,6);
      5: vHTMLDocumento.execCommand('FontSize', False,7);
    end;
end;

procedure TFPrincipal.DBUsuarioTitleClick(Column: TColumn);
var
  I: Integer;
begin
  for I := 0 to DBUsuario.Columns.Count - 1 do
    DBUsuario.Columns[i].Title.Font.Style := [];
  dm.USUARIOS.IndexFieldNames := Column.FieldName;
  Column.Title.Font.Style := [fsBold];
end;

procedure TFPrincipal.DocumentoEmBranco(WebBrowser: TWebBrowser);
begin
    WebBrowser.Navigate('about:blank');
end;

procedure TFPrincipal.EdGroupListEnter(Sender: TObject);
begin
    EdGroupList.Clear;
end;

procedure TFPrincipal.EdGroupListExit(Sender: TObject);
begin
//    EdGroupList.Text:= 'Localizar Group List';
end;

procedure TFPrincipal.EdGroupListKeyPress(Sender: TObject; var Key: Char);
begin
    if Key=#13 then
   begin
       edNome.Text:= 'Localizar Pelo Nome';
       dm.CLIENTES.Close;
       dm.CLIENTES.SQL.Clear;
       dm.CLIENTES.SQL.Add('SELECT * FROM CLIENTE');
       dm.CLIENTES.SQL.Add('WHERE CATEGORIA LIKE '+QuotedStr('%'+EdGroupList.Text+'%'));
       dm.CLIENTES.SQL.Add('ORDER BY CAST(CLIENTEID AS INTEGER)');
       dm.CLIENTES.Open;
   end;
end;

procedure TFPrincipal.edNomeEnter(Sender: TObject);
begin
   edNome.Clear;
end;

procedure TFPrincipal.edNomeExit(Sender: TObject);
begin
//    edNome.Text:= 'Localizar Pelo Nome';
end;

procedure TFPrincipal.edNomeKeyPress(Sender: TObject; var Key: Char);
begin
    if Key=#13 then
   begin
       EdGroupList.Text:= 'Localizar Group List';
       dm.CLIENTES.Close;
       dm.CLIENTES.SQL.Clear;
       dm.CLIENTES.SQL.Add('SELECT * FROM ClIENTE');
       dm.CLIENTES.SQL.Add('WHERE NOME LIKE '+QuotedStr('%'+edNome.Text+'%'));
       dm.CLIENTES.SQL.Add('ORDER BY CAST(CLIENTEID AS INTEGER)');
       dm.CLIENTES.Open;
   end;
end;

procedure TFPrincipal.FormCreate(Sender: TObject);
var
   sHTML : string;

begin
//Navega em uma página em Branco (About:Blank)
   DocumentoEmBranco(webbrowser1);

{altera o modo design do webbrowse para ON, ou seja, toda página que abrirmos poderemos alterar, selecionar, excluir .. inserir ...}
 (WebBrowser1.Document as IHTMLDocument2).designMode := 'On';

{Insere todas as fontes instalados no computador dentro do combobox}

  CbFonte.Items:=screen.Fonts;

   DtAgendamento.DateTime := Now;
   DtAgendamentohora.DateTime := Now;

  sHTML := '<html>'+
           '<body>'+
           '<br/>'+
           '<a></a>' +
           '<b>'+dm.USUARIOSASSINATURA.AsString+'</b>'+
           '</html>'+
           '</body>';
  //         '<img src="http://totalsistemas.info/www/DisparadorEmail/ImagemAss/UsuarioAss/DIOGO LIMA.png" alt=" " width="80" height="80">'+
  WBLoadHTML(WebBrowser1, sHTML);

  dm.UniCategorias.Close;
  dm.UniCategorias.Open;
  dm.CLIENTES.Close;
  dm.CLIENTES.Open;
  dm.ENVIOS.Close;
  dm.ENVIOS.Open;
  dm.CATEGORIA.Close;
  dm.CATEGORIA.Open;
end;

procedure TFPrincipal.FormShow(Sender: TObject);

begin

  if gr.ConectadoInternet = True then
  begin
    LbConexao.Caption      :='';
    LbConexao.font.Color   := clGreen ;
  end;

  edEmail.Text := dm.USUARIOSEMAIL.AsString;

  PcMenuPrincipal.ActivePage := TbPrincipal;

end;

function TFPrincipal.GetIEHandle(WebBrowser: TWebbrowser;
  ClassName: string): HWND;
var
hwndChild, hwndTmp: HWND;
oleCtrl: TOleControl;
szClass: array [0..255] of char;

begin
oleCtrl :=WebBrowser;
hwndTmp := oleCtrl.Handle;
    while (true) do
        begin
        hwndChild := GetWindow(hwndTmp, GW_CHILD);
        GetClassName(hwndChild, szClass, SizeOf(szClass));
            if (string(szClass)=ClassName) then
              begin
              Result :=hwndChild;
              Exit;
              end;
        hwndTmp := hwndChild;
        end;

        Result := 0;
end;

procedure TFPrincipal.GridClientesTitleClick(Column: TColumn);
var
  I: Integer;
begin
  for I := 0 to GridClientes.Columns.Count - 1 do
    GridClientes.Columns[i].Title.Font.Style := [];
  dm.CLIENTES.IndexFieldNames := Column.FieldName;
  Column.Title.Font.Style := [fsBold];
end;

procedure TFPrincipal.Label18Click(Sender: TObject);
begin
  dm.CLIENTES.Close;
  dm.CLIENTES.SQL.Clear;
  dm.CLIENTES.SQL.Add('SELECT * FROM ClIENTE');
  dm.CLIENTES.SQL.Add('WHERE NOME LIKE '+QuotedStr('%'+edNome.Text+'%')+
                       ' AND CATEGORIA LIKE'+QuotedStr('%'+EdGroupList.Text+'%'));
  dm.CLIENTES.Open
end;

procedure TFPrincipal.Label1Click(Sender: TObject);
begin
   PcMenuPrincipal.ActivePage := TbPrincipal;
end;

procedure TFPrincipal.LbTituloDblClick(Sender: TObject);
begin
if InputBox('','Digite a senha do suporte ','')='IMPORTADOR' then
 gr.CriarForm(TFImportador,FImportador);
end;

procedure TFPrincipal.LbMenuClick(Sender: TObject);
begin
  if svMenu.Opened then
  begin
    svMenu.Opened:=false;
    LbMenu.Caption :='';

  end
  else
  begin
    svMenu.Opened:=true;
    LbMenu.Caption :='';

  end;
end;

procedure TFPrincipal.pEnviarClick(Sender: TObject);
begin
 VariosEmails;
 Gr.ChamarAviso('Emails Enviados','Os Emails foram enviados com sucesso','I' );
 AddAnexo.Caption  := '';
 Panexo2.Visible   := False;
 AddAnexo2.Caption := '';
 Panexo3.Visible   := False;
 AddAnexo3.Caption := '';
 SvAnexo.Opened    := False;
 PcMenuPrincipal.ActivePage := TbPrincipal;
end;

procedure TFPrincipal.plimparClick(Sender: TObject);
var
   sHTML : string;
begin
   DocumentoEmBranco(webbrowser1);

   sHTML := '<html>'+
           '<body>'+
           '<br/>'+
           '<a></a>' +
           '<b>'+dm.USUARIOSASSINATURA.AsString+'</b>'+
           '</html>'+
           '</body>';
   WBLoadHTML(WebBrowser1, sHTML);
end;

procedure TFPrincipal.pEditarlistaClick(Sender: TObject);
begin
  dm.CEnvios.edit;
  dm.CENVIOSDATA.AsDateTime     := DtAgendamento.Date;
  dm.CENVIOSHORA.AsDateTime     := DtAgendamentohora.Time;
  dm.CENVIOSUSUARIO.AsInteger   := dm.USUARIOSUSUARIOID.AsInteger;
  dm.CENVIOSCORPOEMAIL.AsString := AnsiToUtf8(WebBrowser1.OleObject.Document.Body.InnerHTML);
  dm.CENVIOSEMAILUSU.AsString   := dm.CLIENTESNOME.AsString;
  dm.CENVIOSENVIADOS.AsString   := 'N';
  dm.CENVIOSASSUNTO.AsString    := EdAssunto.Text;
  dm.CENVIOSEMAILS.AsString     := dm.CLIENTESEMAIL.AsString;
  dm.CENVIOSCATEGORIA.AsString  := vCategorias;
  dm.cEnviosANEXOS.AsString     := IntToStr(dm.UniUltimoAnexoID_ANEXO.AsLargeInt);
  //dm.ENVIOSID_ANEXO.AsLargeInt := dm.UniUltimoAnexoID_ANEXO.AsLargeInt;
  dm.CEnvios.Post;

  GR.CriarForm(TDisparador,Disparador);
  SvAnexo.Opened    := False;
  PcMenuPrincipal.ActivePage := TbPrincipal;
end;

procedure TFPrincipal.AddAnexo2Click(Sender: TObject);
var
  vCaminho,vNome :String;
begin
  if OpenDialog.Execute then
   begin
    vcaminho := OpenDialog.FileName;
    vNome    := ExtractFileName(OpenDialog.FileName);
   end;

 dm.IdFTP.Disconnect;
 dm.IdFTP.Connect;
 //dm.IdFTP.ChangeDir('www/DisparadorEmail/Anexo/');
 dm.IdFTP.Put(vCaminho,'www/DisparadorEmail/Anexo/Recebido/'+vNome,true);
 dm.IdFtp.Disconnect;
 vANEXO :='www/DisparadorEmail/Anexo/Recebido/'+vNome;
 AddAnexo2.Caption := vNome;
 Panexo3.Visible := true;
end;

procedure TFPrincipal.AddAnexo3Click(Sender: TObject);
var
 vCaminho,vNome :String;
begin
  if OpenDialog.Execute then
   begin
    vcaminho := OpenDialog.FileName;
    vNome    := ExtractFileName(OpenDialog.FileName);
   end;

 dm.IdFTP.Disconnect;
 dm.IdFTP.Connect;
 //dm.IdFTP.ChangeDir('www/DisparadorEmail/Anexo/');
 dm.IdFTP.Put(vCaminho,'www/DisparadorEmail/Anexo/Recebido/'+vNome,true);
 dm.IdFtp.Disconnect;
 vANEXO :='www/DisparadorEmail/Anexo/Recebido/'+vNome;
 AddAnexo3.Caption := vNome;
 Panexo4.Visible := true;
end;

procedure TFPrincipal.AddAnexo4Click(Sender: TObject);
var
 vCaminho,vNome :String;
begin
  if OpenDialog.Execute then
   begin
    vcaminho := OpenDialog.FileName;
    vNome    := ExtractFileName(OpenDialog.FileName);
   end;

 dm.IdFTP.Disconnect;
 dm.IdFTP.Connect;
 //dm.IdFTP.ChangeDir('www/DisparadorEmail/Anexo/');
 dm.IdFTP.Put(vCaminho,'www/DisparadorEmail/Anexo/Recebido/'+vNome,true);
 dm.IdFtp.Disconnect;
 vANEXO :='www/DisparadorEmail/Anexo/Recebido/'+vNome;
 AddAnexo4.Caption := vNome;
 Panexo5.Visible := true;
end;

procedure TFPrincipal.AddAnexo5Click(Sender: TObject);
var
 vCaminho,vNome :String;
begin
  if OpenDialog.Execute then
   begin
    vcaminho := OpenDialog.FileName;
    vNome    := ExtractFileName(OpenDialog.FileName);
   end;

 dm.IdFTP.Disconnect;
 dm.IdFTP.Connect;
 //dm.IdFTP.ChangeDir('www/DisparadorEmail/Anexo/');
 dm.IdFTP.Put(vCaminho,'www/DisparadorEmail/Anexo/Recebido/'+vNome,true);
 dm.IdFtp.Disconnect;
 vANEXO :='www/DisparadorEmail/Anexo/Recebido/'+vNome;
 AddAnexo5.Caption := vNome;
 Panexo6.Visible := true;
end;

procedure TFPrincipal.AddAnexo6Click(Sender: TObject);
var
 vCaminho,vNome :String;
begin
  if OpenDialog.Execute then
   begin
    vcaminho := OpenDialog.FileName;
    vNome    := ExtractFileName(OpenDialog.FileName);
   end;

 dm.IdFTP.Disconnect;
 dm.IdFTP.Connect;
 //dm.IdFTP.ChangeDir('www/DisparadorEmail/Anexo/');
 dm.IdFTP.Put(vCaminho,'www/DisparadorEmail/Anexo/Recebido/'+vNome,true);
 dm.IdFtp.Disconnect;
 vANEXO :='www/DisparadorEmail/Anexo/Recebido/'+vNome;
 AddAnexo6.Caption := vNome;
 Panexo7.Visible := true;
end;

procedure TFPrincipal.AddAnexo7Click(Sender: TObject);
var
 vCaminho,vNome :String;
begin
  if OpenDialog.Execute then
   begin
    vcaminho := OpenDialog.FileName;
    vNome    := ExtractFileName(OpenDialog.FileName);
   end;

 dm.IdFTP.Disconnect;
 dm.IdFTP.Connect;
 //dm.IdFTP.ChangeDir('www/DisparadorEmail/Anexo/');
 dm.IdFTP.Put(vCaminho,'www/DisparadorEmail/Anexo/Recebido/'+vNome,true);
 dm.IdFtp.Disconnect;
 vANEXO :='www/DisparadorEmail/Anexo/Recebido/'+vNome;
 AddAnexo7.Caption := vNome;
end;

procedure TFPrincipal.AddAnexoClick(Sender: TObject);
var
 vCaminho,vNome :String;
begin
  if OpenDialog.Execute then
   begin
    vcaminho := OpenDialog.FileName;
    vNome    := ExtractFileName(OpenDialog.FileName);
   end;

 dm.IdFTP.Disconnect;
 dm.IdFTP.Connect;
 //dm.IdFTP.ChangeDir('www/DisparadorEmail/Anexo/');
 dm.IdFTP.Put(vCaminho,'www/DisparadorEmail/Anexo/Recebido/'+vNome,true);
 dm.IdFtp.Disconnect;
 vANEXO :='www/DisparadorEmail/Anexo/Recebido/'+vNome;
 AddAnexo.Caption := vNome;
 Panexo2.Visible  := true;
end;

procedure TFPrincipal.Panel1Click(Sender: TObject);
var
   sHTML : string;

begin
 PcMenuPrincipal.ActivePage := TbEnvio;
 CbFonte.Visible            := true;
 CbTamanho.Visible          := true;
 EdAssunto.Text             := '';
 SvAnexo.Opened             := False;
 DocumentoEmBranco(webbrowser1);

   sHTML := '<html>'+
           '<body>'+
           '<br/>'+
           '<a></a>' +
           '<b>'+dm.USUARIOSASSINATURA.AsString+'</b>'+
           '</html>'+
           '</body>';
   WBLoadHTML(WebBrowser1, sHTML);


 dm.mAnexos.Close;
 dm.mAnexos.Open;
end;

procedure TFPrincipal.Panel5Click(Sender: TObject);
begin
  dm.Clientes.Append;
  gr.CriarForm(TFCadastroClientes,FCadastroClientes );
end;

procedure TFPrincipal.Panel6Click(Sender: TObject);
begin
  dm.CLIENTES.Edit;
  gr.CriarForm(TFCadastroClientes,FCadastroClientes );
end;

procedure TFPrincipal.Panel7Click(Sender: TObject);
begin
    if GR.Confirma('Deseja mesmo excluir esse registro?') then
    begin
        dm.CLIENTES.Delete;
    end;
end;

procedure TFPrincipal.PnAnexoClick(Sender: TObject);
begin
  GR.CriarForm(TFCadastroAnexo,FCadastroAnexo);
{  if SvAnexo.Opened then
  begin
    SvAnexo.Opened:=false;
  end
  else
  begin
    SvAnexo.Opened:=true;
  end;  }
end;

procedure TFPrincipal.PnCadastroClick(Sender: TObject);
begin
    PcMenuPrincipal.ActivePage := TbCadCli;
    AtualizarTelaCli;
    EdGroupList.Text:= 'Localizar Group List';
    edNome.Text:= 'Localizar Pelo Nome';
end;

procedure TFPrincipal.PnCadUSUClick(Sender: TObject);
begin
    PcMenuPrincipal.ActivePage := TbCadUsu;
end;

procedure TFPrincipal.PnCentroClick(Sender: TObject);
begin
    SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
    WM_COMMAND,IDM_CENTRALIZAR, 0);
end;

procedure TFPrincipal.PnCorClick(Sender: TObject);
begin
    vHTMLDocumento := WebBrowser1.Document as IHTMLDocument2;
    if Colordialog.Execute then
        vHTMLDocumento.execCommand('ForeColor', False,ColorDialog.Color)
    else
    abort;
end;

procedure TFPrincipal.PnCrescClick(Sender: TObject);
begin
    SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
    WM_COMMAND,IDM_INDENT, 0);
end;

procedure TFPrincipal.PnDecresClick(Sender: TObject);
begin
    SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
    WM_COMMAND,IDM_OUTDENT, 0);
end;

procedure TFPrincipal.PnDireitaClick(Sender: TObject);
begin
    SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
    WM_COMMAND,IDM_ALINHADIR, 0);
end;

procedure TFPrincipal.PnEsquerClick(Sender: TObject);
begin
    SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
    WM_COMMAND,IDM_ALINHARESQ, 0);
end;

procedure TFPrincipal.PnFotoClick(Sender: TObject);
begin
    SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
    WM_COMMAND,IDM_IMAGEM, 0);
end;

procedure TFPrincipal.PnHiperLinkClick(Sender: TObject);
begin
   SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
   WM_COMMAND,IDM_HYPERLINK,0);
end;

procedure TFPrincipal.PnItalicoClick(Sender: TObject);
begin
   BorderStyle    := bsSingle;
   vHTMLDocumento := WebBrowser1.Document as IHTMLDocument2;
   vHTMLDocumento.execCommand('Italic', False,0);
end;

procedure TFPrincipal.PnlAlterarUsuarioClick(Sender: TObject);
begin
  dm.USUARIOS.edit;
  gr.CriarForm(TCadastroUsuario,CadastroUsuario);
end;

procedure TFPrincipal.PnlExcluirUsuarioClick(Sender: TObject);
begin
    if GR.Confirma('Deseja mesmo excluir esse registro?') then
    begin
        dm.USUARIOS.Delete;
    end;
end;

procedure TFPrincipal.PnLImparClick(Sender: TObject);
begin
    BorderStyle    := bsSingle;
    SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
    WM_COMMAND,IDM_DESFAZER,0);
end;

procedure TFPrincipal.PnlIncluirUsuarioClick(Sender: TObject);
begin
  dm.USUARIOS.Append;
  gr.CriarForm(TCadastroUsuario,CadastroUsuario);
end;

procedure TFPrincipal.PnLinhaHorizontalClick(Sender: TObject);
begin
     SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
     WM_COMMAND,IDM_LINHAHORIZ, 0);
end;

procedure TFPrincipal.PnMarcadorClick(Sender: TObject);
begin
    SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
    WM_COMMAND,IDM_MARCADOR_LISTA, 0);
end;

procedure TFPrincipal.PnNegritoClick(Sender: TObject);
begin
   BorderStyle    := bsSingle;
{Aqui estamos dizendo que HTMLDocumento = A Interface que comanda o design do webbrowser, a interface que irá inserir em seu texto selecionado um negrito... italico..}
   vHTMLDocumento := WebBrowser1.Document as IHTMLDocument2;

{Estamos falando oq queremos aplicar no texto selecionado, então dizemos Bold ou seja Negrito}
   vHTMLDocumento.execCommand('Bold', False,0);
end;

procedure TFPrincipal.PnNunListClick(Sender: TObject);
begin
   SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
   WM_COMMAND,IDM_MARCADOR, 0);
end;

procedure TFPrincipal.PnRelClick(Sender: TObject);
begin
    PcMenuPrincipal.ActivePage := TbRelatorio;
end;

procedure TFPrincipal.PnSublinhadoClick(Sender: TObject);
begin
    vHTMLDocumento := WebBrowser1.Document as IHTMLDocument2;
    vHTMLDocumento.execCommand('Underline', False,0);
end;

procedure TFPrincipal.SbFecharClick(Sender: TObject);
begin
    Application.Terminate;
end;

procedure TFPrincipal.SbMinimizarClick(Sender: TObject);
begin
    Application.Minimize;
end;

procedure TFPrincipal.TbPrincipalContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
begin
//
end;

function TFPrincipal.UltimoCliente: Integer;
begin
  dm.CLIENTES.Close;
  dm.CLIENTES.SQL.Clear;
  dm.CLIENTES.SQL.Add('select MAX(CLIENTEID) as NUM from cliente') ;
  dm.CLIENTES.Open;

  Result:= dm.CLIENTES.FieldByName('NUM').AsInteger;
end;

procedure TFPrincipal.VariosEmails;
Var
  Data,hora : string;
  i: Integer;
  vListArquivos:TStringList;
  vNomeArquivo:String;
begin
 try
   dm.Clientes.Close;
   dm.Clientes.Open;

   dm.UniCategorias.First;
   dm.CLIENTES.Filter:='';
   vCategorias:='';

   dm.CLIENTES.Filtered:=False;

   while not dm.UniCategorias.Eof do
   begin
     if  dm.UniCategoriasXCHECK.AsInteger = 1 then
     begin
          if  dm.CLIENTES.Filter='' then
          begin
            dm.CLIENTES.Filter:='(CATEGORIA='+QuotedStr(dm.UniCategoriasCATEGORIA.AsString)+')';
          end
          else
            dm.CLIENTES.Filter:=dm.CLIENTES.Filter + ' OR (CATEGORIA='+QuotedStr(dm.UniCategoriasCATEGORIA.AsString)+')';
          if vCategorias='' then
            vCategorias:= sLineBreak+dm.UniCategoriasCATEGORIA.AsString
          else
            vCategorias:=vCategorias+sLineBreak+dm.UniCategoriasCATEGORIA.AsString
     end;

     dm.UniCategorias.Next;
    end;

    dm.CLIENTES.Filtered:=True;;

    if not dm.CLIENTES.IsEmpty then
    begin
      dm.mAnexos.First;

      vListArquivos:=TStringList.Create;

      while not dm.mAnexos.Eof do
      begin
        if Trim(vListArquivos.Text)='' then
          vListArquivos.Text:=ExtractFileName(dm.mAnexosPATH.AsString)
        else
          vListArquivos.Text:=vListArquivos.Text+'|'+ExtractFileName(dm.mAnexosPATH.AsString);

        dm.mAnexos.Next;
      end;

      dm.UniAnexos.Close;
      dm.UniAnexos.Open;

      dm.UniAnexos.Append;
      dm.UniAnexosANEXOS.AsString:=vListArquivos.Text;
      dm.UniAnexos.Post;


      dm.UniUltimoAnexo.Close;
      dm.UniUltimoAnexo.Open;


      dm.IdFTP.Disconnect;
      dm.IdFTP.Connect;

      if dm.IdFTP.Connected then
      begin
          if not dm.FTPDirExistsTry('www/DisparadorEmail/Anexo/Recebido/'+dm.UniUltimoAnexoID_ANEXO.AsString+'/',dm.IdFTP) then
          begin
              dm.IdFTP.MakeDir('www/DisparadorEmail/Anexo/Recebido/'+dm.UniUltimoAnexoID_ANEXO.AsString+'/');
              dm.IdFTP.ChangeDir('www/DisparadorEmail/Anexo/Recebido/'+dm.UniUltimoAnexoID_ANEXO.AsString+'/');
          end;

          dm.mAnexos.First;

          while not dm.mAnexos.Eof do
          begin
            vNomeArquivo    := ExtractFileName(dm.mAnexosPATH.AsString);
            dm.IdFTP.Put(dm.mAnexosPATH.AsString,vNomeArquivo,true);

            dm.mAnexos.Next;
          end;
      end;

      dm.CLIENTES.First;

      while not dm.CLIENTES.Eof do
      begin
         dm.ENVIOS.Append;
         dm.ENVIOSDATA.AsDatetime     := DtAgendamento.Date;
         dm.ENVIOSHORA.AsDateTime     := DtAgendamentohora.Time;
         dm.ENVIOSUSUARIO.AsInteger   := dm.USUARIOSUSUARIOID.AsInteger;
         dm.ENVIOSCORPOEMAIL.AsString := AnsiToUtf8(WebBrowser1.OleObject.Document.Body.InnerHTML);
         dm.ENVIOSPERSON.AsString     := dm.CLIENTESPERSON.AsString;
         dm.ENVIOSENVIADOS.AsString   := 'N';
         dm.ENVIOSCLIENTEID.AsInteger := dm.CLIENTESCLIENTEID.AsInteger;
         dm.ENVIOSEMAILS.AsString     := dm.CLIENTESEMAIL.AsString;
         dm.ENVIOSASSUNTO.AsString    := EdAssunto.Text;
         dm.ENVIOSCATEGORIA.AsString  := vCategorias;
         dm.ENVIOSANEXOS.AsString     := IntToStr(dm.UniUltimoAnexoID_ANEXO.AsLargeInt);
         dm.ENVIOSID_ANEXO.AsLargeInt := dm.UniUltimoAnexoID_ANEXO.AsLargeInt;
         dm.ENVIOS.Post;
        dm.CLIENTES.Next;
      end;
    end;
 finally
   FreeAndNil(vListArquivos)
 end;
end;

procedure TFPrincipal.WBLoadHTML(WebBrowser: TWebBrowser; HTMLCode: string);
var
   sl: TStringList;
   ms: TMemoryStream;
begin
   WebBrowser1.Navigate('about:blank') ;
   while WebBrowser1.ReadyState < READYSTATE_INTERACTIVE do
    Application.ProcessMessages;
   (WebBrowser1.Document as IHTMLDocument2).designMode := 'On';
   if Assigned(WebBrowser1.Document) then
   begin
     sl := TStringList.Create;
     try
       ms := TMemoryStream.Create;
       try
         sl.Text := HTMLCode;
         sl.SaveToStream(ms) ;
         ms.Seek(0, 0) ;
         (WebBrowser1.Document as IPersistStreamInit).Load(TStreamAdapter.Create(ms)) ;
       finally
         ms.Free;
       end;
     finally
       sl.Free;
     end;
   end;
end;

procedure TFPrincipal.wwDBGrid1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if dm.UniCategorias.IsEmpty then Exit;

  if Screen.Cursor=crHandPoint then
  begin
    dm.UniCategorias.Edit;
    if dm.UniCategoriasXCHECK.AsInteger=1 then
      dm.UniCategoriasXCHECK.AsInteger:=0
    else
      dm.UniCategoriasXCHECK.AsInteger:=1;
    dm.UniCategorias.Post;
  end;
  pEnviar.Enabled := True;
  pEditarlista.Enabled := True;

end;

procedure TFPrincipal.wwDBGrid1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if X in [1..24] then
    Screen.Cursor:=crHandPoint
  else
    Screen.Cursor:=crDefault;
end;

end.
